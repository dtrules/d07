
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Customization;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class CustomizationServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private CustomizationService	customizationService;


	// Templates --------------------------------------------------------------

	/*
	 * Vista para editar un Customization
	 * 
	 * En este caso de uso se permite al administrador editar el customization
	 * Para forzar el error pueden darse los siguientes casos:
	 * 
	 * � El usuario no esta autentificado como administrador
	 * � Atributos del registro incorrectos
	 */
	public void editCustomizationTest(final String username, final String header, final String welcomeMessage, final Collection<String> spamWords, final double vat, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Obtenemos el customization actual
			final Customization customization = this.customizationService.findCustomization();

			customization.setHeader(header);
			customization.setWelcomeMessage(welcomeMessage);
			customization.setSpamWords(spamWords);
			customization.setVat(vat);

			// Comprobamos atributos
			this.customizationService.checkCustomization(customization);

			// Guardamos
			this.customizationService.save(customization);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverEditCustomizationTest() {

		final Collection<String> spamWords = new ArrayList<String>();
		spamWords.add("Test");

		final Object testingData[][] = {
			// Editar customization sin estar autentificado -> false
			{
				null, "header.jpg", "WelcomeToCustomizationest", spamWords, 28.2, IllegalArgumentException.class
			},
			// Editar customization autentificado como customer -> false
			{
				"customer1", "header.jpg", "WelcomeToCustomizationest", spamWords, 28.2, IllegalArgumentException.class
			},
			// Editar customization autentificado como handyWorker -> false
			{
				"handyworker1", "header.jpg", "WelcomeToCustomizationest", spamWords, 28.2, IllegalArgumentException.class
			},
			// Editar customization autentificado como referee -> false
			{
				"referee1", "header.jpg", "WelcomeToCustomizationest", spamWords, 28.2, IllegalArgumentException.class
			},
			// Editar customization autentificado como sponsor -> false
			{
				"sponsor1", "header.jpg", "WelcomeToCustomizationest", spamWords, 28.2, IllegalArgumentException.class
			},
			// Editar customization como administrador y atributos v�lidos -> false
			{
				"admin", "header.jpg", "WelcomeToCustomizationest", spamWords, 28.2, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.editCustomizationTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Collection<String>) testingData[i][3], (double) testingData[i][4], (Class<?>) testingData[i][5]);
	}
}
