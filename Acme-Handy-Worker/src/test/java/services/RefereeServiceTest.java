
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;
import domain.Referee;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class RefereeServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private RefereeService	refereeService;

	@Autowired
	private ActorService	actorService;


	// Templates --------------------------------------------------------------

	public void listRefereeTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.refereeService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Registro de referee
	 * 
	 * En este caso de uso se llevara a cabo el registro de un sponsor en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario esta autentificado y no es admin
	 * � Atributos del registro incorrectos
	 * � Nombre de usuario ya existente
	 */
	public void registerReferee(final String username, final String newUsername, final String password, final String name, final String middleName, final String surname, final String photo, final String email, final String phoneNumber,
		final String address, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que este autentificado
			this.actorService.checkAuth(Authority.ADMIN);

			// Inicializamos los atributos para la creaci�n
			final Referee referee = this.refereeService.create();

			referee.setName(name);
			referee.setMiddleName(middleName);
			referee.setSurname(surname);
			referee.setPhoto(photo);
			referee.setEmail(email);
			referee.setPhoneNumber(phoneNumber);
			referee.setAddress(address);

			final UserAccount ua = referee.getUserAccount();
			ua.setUsername(newUsername);
			ua.setPassword(password);
			referee.setUserAccount(ua);

			// Comprobamos atributos
			this.refereeService.checkReferee(referee);

			// Guardamos
			this.refereeService.save(referee);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverListRefereeTest() {
		final Object testingData[][] = {
			// Se accede con admin -> true
			{
				"admin", null
			},
			// Se accede con sponsor -> true
			{
				"referee1", null
			},
			//  Se accede con usuario no autentificado -> true
			{
				null, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listRefereeTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverRegisterReferee() {

		final Object testingData[][] = {
			// Registrar referee sin estar autentificado -> true
			{
				null, "referee3", "referee3", "Referee3Name", "referee3middlename", "referee3surname", "www.linkedin.com/testpicture.jpg", "referee3@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Registrar referee estando autentificado -> true
			{
				"admin", "referee3", "referee3", "referee3Name", "referee3middlename", "referee3surname", "www.linkedin.com/testpicture.jpg", "sponsor3@gmail.com", "612239922", "Abbey Road, 21", null
			},
			// Registrar referee estando autentificado -> false
			{
				"customer1", "referee3", "referee3", "Referee3Name", "referee3middlename", "referee3surname", "www.linkedin.com/testpicture.jpg", "referee3@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerReferee((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (String) testingData[i][8], (String) testingData[i][9], (Class<?>) testingData[i][10]);
	}

}
