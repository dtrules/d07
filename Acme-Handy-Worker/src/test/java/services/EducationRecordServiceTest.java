
package services;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.EducationRecord;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class EducationRecordServiceTest extends AbstractTest {

	//Service under test ---------------------------
	@Autowired
	private EducationRecordService	educationRecordService;
	@Autowired
	private HandyWorkerService		handyWorkerService;


	// Test ------------------------

	//Caso en que se crea un EducationRecord correctamente y guarda datos

	public void createEducationRecord(final String username, final Date startMoment, final Date endMoment, final Collection<String> comments, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es handy worker
			this.handyWorkerService.checkIfHandyWorker();

			final EducationRecord educationRecord = this.educationRecordService.create();
			educationRecord.setStartMoment(startMoment);
			educationRecord.setEndMoment(endMoment);
			educationRecord.setComments(comments);

			this.educationRecordService.save(educationRecord);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateEducationRecord() {
		final Date start = new GregorianCalendar(2018, Calendar.OCTOBER, 10).getTime();
		final Date end = new GregorianCalendar(2018, Calendar.NOVEMBER, 10).getTime();

		final Date start1 = new GregorianCalendar(2018, Calendar.DECEMBER, 12).getTime();
		final Date end1 = new GregorianCalendar(2019, Calendar.DECEMBER, 20).getTime();

		final Date start2 = new GregorianCalendar(2018, Calendar.OCTOBER, 12).getTime();
		final Date end2 = new GregorianCalendar(2018, Calendar.OCTOBER, 3).getTime();

		final Object testingData[][] = {
			// Crear y guardar una EducationRecord con handyWorker autenticado
			{
				"handyWorker1", start, end, Collections.<String> emptySet(), null
			},
			// Crear y guardar un EducationRecord con fecha inicio despues de la actual
			{
				"handyWorker2", start1, end1, Collections.<String> emptySet(), IllegalArgumentException.class
			},
			// Crear y guardar una EducationRecord con fecha inicio despues de fecha fin
			{
				"handyWorker1", start2, end2, Collections.<String> emptySet(), IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.createEducationRecord((String) testingData[i][0], (Date) testingData[i][1], (Date) testingData[i][2], (Collection<String>) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	//Caso en el que borramos un educationRecord

	public void deleteEducationRecord(final String username, final int educationRecordId, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el educationRecord
			final EducationRecord educationRecord = this.educationRecordService.findOne(educationRecordId);

			//Borramos
			this.educationRecordService.delete(educationRecord);

			//Comprobamos que se ha borrado

			Assert.isNull(this.educationRecordService.findOne(educationRecordId));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
}
