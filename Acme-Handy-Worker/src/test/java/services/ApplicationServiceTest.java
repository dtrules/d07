
package services;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Application;
import domain.CreditCard;
import domain.Status;
import domain.Task;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class ApplicationServiceTest extends AbstractTest {

	// Service under test ---------------------------

	@Autowired
	private ApplicationService	applicationService;
	@Autowired
	private HandyWorkerService	handyWorkerService;
	@Autowired
	private CustomerService		customerService;
	@Autowired
	private TaskService			taskService;


	//Tests ------------------------------

	// Caso en que se crea un Application correctamente y guarda datos siendo Customer

	public void editApplicationAsCustomer(final String username, final Status status, final CreditCard creditCard, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			// Comprobamos si la persona autenticada es customer
			this.customerService.checkIfCustomer();

			final Application application = (Application) this.applicationService.findAll().toArray()[0];
			application.setStatus(status);

			this.applicationService.save(application);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverEditApplicationAsCustomer() {

		final Status status = new Status();
		status.setValue("ACCEPTED");
		final CreditCard creditCard = new CreditCard();

		final Object testingData[][] = {
			// Crear y guardar una application con customer autenticado
			{
				"customer1", status, creditCard, null
			},

			//crear y guardar un application con otro actor -> false

			{
				"sponsor1", status, creditCard, IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.editApplicationAsCustomer((String) testingData[i][0], (Status) testingData[i][1], (CreditCard) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	// Caso en que se crea un Application correctamente y guarda datos siendo HandyWorker

	public void createApplicationAsHandyWorker(final String username, final Date moment, final Status status, final double price, final Collection<String> comments, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			// Comprobamos si la persona autenticada es handy worker
			this.handyWorkerService.checkIfHandyWorker();

			final Application application = this.applicationService.create();
			final Task task = (Task) this.taskService.findAll().toArray()[0];

			application.setTask(task);
			application.setMoment(moment);
			application.setPrice(price);
			application.setStatus(status);
			application.setComments(comments);

			this.applicationService.save(application);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateApplicationAsHandyWorker() {
		final Date moment = new GregorianCalendar(2018, Calendar.NOVEMBER, 29).getTime();
		final Date moment2 = new GregorianCalendar(2021, Calendar.NOVEMBER, 29).getTime();

		final Status status = new Status();
		status.setValue("ACCEPTED");

		final Object testingData[][] = {
			// Crear y guardar una application con handyworker autenticado
			{
				"handyWorker1", moment, status, 15.5, Collections.<String> emptySet(), null
			},
			// Crear y guardar una application con un handy worker y con una
			// fecha de publicación posterior a la actual -> false
			{
				"handyWorker2", moment2, status, 25.5, Collections.<String> emptySet(), IllegalArgumentException.class
			},

			//crear y guardar un application con otro actor -> false

			{
				"sponsor1", moment, status, 25.5, Collections.<String> emptySet(), IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.createApplicationAsHandyWorker((String) testingData[i][0], (Date) testingData[i][1], (Status) testingData[i][2], (double) testingData[i][3], (Collection<String>) testingData[i][4], (Class<?>) testingData[i][5]);
	}

	public void deleteApplication(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			// Buscamos la Aplication
			final Application application = (Application) this.applicationService.findAll().toArray()[0];

			// Borramos
			this.applicationService.delete(application);

			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverDeleteApplicationTest() {
		final Object testingData[][] = {
			// Se accede con handy worker -> true
			{
				"handyWorker1", IllegalArgumentException.class
			},
			// Se accede con customer -> true
			{
				"customer1", null
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteApplication((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
}
