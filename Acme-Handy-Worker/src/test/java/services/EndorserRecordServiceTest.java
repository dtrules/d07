
package services;

import java.util.Collection;
import java.util.Collections;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.EndorserRecord;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class EndorserRecordServiceTest extends AbstractTest {

	//Service under test ---------------------------

	@Autowired
	private EndorserRecordService	endorserRecordService;
	@Autowired
	private HandyWorkerService		handyWorkerService;


	// Test ------------------------

	//Caso en que se crea un EndorserRecord correctamente y guarda datos

	public void createEndorserRecord(final String username, final Collection<String> comments, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es handy worker
			this.handyWorkerService.checkIfHandyWorker();

			final EndorserRecord endorserRecord = this.endorserRecordService.create();
			endorserRecord.setComments(comments);

			this.endorserRecordService.save(endorserRecord);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateEndorserRecord() {

		final Object testingData[][] = {
			// Crear y guardar una EndorserRecord con handy worker autenticado
			{
				"handyWorker1", Collections.<String> emptySet(), null
			},
			// Crear y guardar un EndorserRecord con otro actor
			{
				"customer1", Collections.<String> emptySet(), IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.createEndorserRecord((String) testingData[i][0], (Collection<String>) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	//	//Caso en el que borramos un EndorserRecord
	//
	//	public void deleteEndorserRecord(final String username, final int endorserRecordId, final Class<?> expected) {
	//		Class<?> caught = null;
	//		try {
	//
	//			this.authenticate(username);
	//
	//			//Buscamos el educationRecord
	//			final EndorserRecord endorserRecord = this.endorserRecordService.findOne(endorserRecordId);
	//
	//			//Borramos
	//			this.endorserRecordService.delete(endorserRecord);
	//
	//			//Comprobamos que se ha borrado
	//
	//			Assert.isNull(this.endorserRecordService.findOne(endorserRecordId));
	//			this.unauthenticate();
	//		}
	//
	//		catch (final Throwable oops) {
	//
	//			caught = oops.getClass();
	//		}
	//
	//		this.checkExceptions(expected, caught);
	//	}
}
