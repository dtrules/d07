
package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Profile;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class ProfileServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private ProfileService	profileService;

	@Autowired
	private ActorService	actorService;


	// Templates --------------------------------------------------------------

	/*
	 * Vista para ver todos los Profiles
	 * 
	 * En este caso de uso se listan los profiles desde su correspondiente vista
	 * � El usuario no esta autentificado
	 */
	public void listProfileTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.profileService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Registro de profile
	 * 
	 * En este caso de uso se llevara a cabo el registro de un profile en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario no esta autentificado
	 * � Atributos del registro incorrectos
	 */
	public void registerProfileTest(final String username, final String nick, final String socialNetwork, final String link, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Inicializamos los atributos para la creaci�n
			final Profile profile = this.profileService.create();

			profile.setNick(nick);
			profile.setSocialNetwork(socialNetwork);
			profile.setLink(link);

			// Comprobamos atributos
			this.profileService.checkProfile(profile);

			// Guardamos
			this.profileService.save(profile);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Borrado de profile
	 * 
	 * En este caso de uso se llevara a cabo el borrado de un profile en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario no esta autentificado como profile
	 * � El profile no existe
	 */
	public void deleteProfileTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);
			Assert.notNull(this.actorService.findByPrincipal());

			// Buscamos el profile
			final Collection<Profile> profiles = this.actorService.findByPrincipal().getProfiles();

			// Borramos
			final Profile p = (Profile) profiles.toArray()[0];
			this.profileService.delete(p);

			// Comprobamos que se ha borrado

			Assert.isNull(this.profileService.findOne(p.getId()));

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverListProfileTest() {
		final Object testingData[][] = {
			// Se accede con sponsor -> true
			{
				"sponsor1", null
			},
			// Se accede con sponsor -> true
			{
				"sponsor2", null
			},
			//  Se accede con admin -> true
			{
				"admin", null
			},
			//  Se accede con customer -> true
			{
				"customer1", null
			},
			//  Se accede con handyworker -> true
			{
				"handyWorker1", null
			},
			//  Se accede con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listProfileTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverRegisterProfileTest() {

		final Object testingData[][] = {
			// Registrar profile sin estar autentificado -> false
			{
				null, "nickTest", "socialNetworkTest", "http://www.linktest.com", IllegalArgumentException.class
			},
			// Registrar profile con datos no v�lidos -> false
			{
				"handyWorker1", "nickTest", null, "http://www.linktest.com", IllegalArgumentException.class
			},
			// Registrar profile con datos no v�lidos -> false
			{
				"handyWorker1", "nickTest", "socialNetworktest", null, IllegalArgumentException.class
			},
			// Registrar profile autentificado como handyworker -> true
			{
				"handyWorker1", "nickTest", "socialNetworkTest", "http://www.linktest.com", null
			},
			// Registrar profile autentificado como sponsor -> true
			{
				"sponsor1", "nickTest", "socialNetworkTest", "http://www.linktest.com", null
			},
			// Registrar profile autentificado como customer -> true
			{
				"customer1", "nickTest", "socialNetworkTest", "http://www.linktest.com", null
			},
			// Registrar profile autentificado como admin -> true
			{
				"admin", "nickTest", "socialNetworkTest", "http://www.linktest.com", null
			},
			// Registrar profile autentificado como referee -> true
			{
				"referee1", "nickTest", "socialNetworkTest", "http://www.linktest.com", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerProfileTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void driverDeleteProfileTest() {
		final Object testingData[][] = {

			//			//  Se accede con customer -> true
			//			{
			//				"customer1", null
			//			},
			//  Se accede con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class

			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteProfileTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
}
