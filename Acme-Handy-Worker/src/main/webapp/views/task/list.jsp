<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!--Tabla-->

<fieldset>
	<legend>
		<spring:message code="task.searchText" />
	</legend>
	<form action="task/list.do" method="GET">
		<input type="text" name="searchContent" /> <input type="submit"
			value="<spring:message code = "task.searchContent" />" />
	</form>
	<form action="task/list.do" method="GET">
		<input type="text" name="searchCategory" /> <input type="submit"
			value="<spring:message code = "task.searchCategory" />" />
	</form>
	<form action="task/list.do" method="GET">
		<input type="text" name="searchWarranty" /> <input type="submit"
			value="<spring:message code = "task.searchWarranty" />" />
	</form>
</fieldset>

<display:table name="tasks" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	
	<!-- La lista con el bot�n de editar en cada fila-->

	<display:column>
		<a href="task/display.do?taskId=${row.id}">
			<spring:message code="task.display"/>
		</a>
	</display:column>
	
	<spring:message code="task.ticker" var="tickerHeader"/>
	<display:column property="ticker" title="${tickerHeader}" sortable="true" />
	
	<spring:message code="task.publishDate" var="publishDateHeader"/>
	<display:column property="publishDate" title="${publishDateHeader}" sortable="true" />
	
	<spring:message code="task.description" var="descriptionHeader"/>
	<display:column property="description" title="${descriptionHeader}" sortable="true" />

	<spring:message code="task.address" var="addressHeader"/>
	<display:column property="address" title="${addressHeader}" sortable="true" />

	<spring:message code="task.maxPrice" var="maxPriceHeader"/>
    <display:column property="maxPrice" title="${maxPriceHeader}" sortable="true" />

	<spring:message code="task.startMoment" var="startMomentHeader"/>
    <display:column property="startMoment" title="${startMomentHeader}" sortable="true" />
	
	<spring:message code="task.endMoment" var="endMomentHeader"/>
    <display:column property="endMoment" title="${endMomentHeader}" sortable="true" />
    
    <security:authorize access="hasRole('CUSTOMER')">
	    <display:column>
			<a href="task/customer/edit.do?taskId=${row.id}">
				<spring:message code="task.edit"/>
			</a>
		</display:column>
	    <display:column>
			<a href="task/customer/delete.do?taskId=${row.id}">
				<spring:message code="task.delete"/>
			</a>
		</display:column>
	</security:authorize>

</display:table>



<!--Bot�n de crear debajo de la lista-->

<security:authorize access="hasRole('CUSTOMER')">
	<div>
		<a href="task/customer/create.do">
			<spring:message code="task.create"/>
		</a>
	</div>
</security:authorize>
