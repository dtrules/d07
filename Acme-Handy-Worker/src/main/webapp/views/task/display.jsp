<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<p><spring:message code="task.display" /></p>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<h2> <spring:message code="task.display"/> </h2>

<spring:message code="task.date.pattern" var="datePattern"/>
	
	<!-- Attributes -->

	<b><spring:message code="task.ticker"/></b>: <jstl:out value="${task.ticker}"/>
	<br/>
	<b><spring:message code="task.publishDate"/></b>: <fmt:formatDate value="${task.publishDate}" pattern="${datePattern}" />
	<br/>
	<b><spring:message code="task.description"/></b>: <jstl:out value="${task.description}"/>
	<br/>
	<b><spring:message code="task.address"/></b>: <jstl:out value="${task.address}"/>
	<br/>
	<b><spring:message code="task.maxPrice"/></b>: <jstl:out value="${task.maxPrice}"/> (<jstl:out value="${newTaskPrice}"/>)
	<br/>
	<b><spring:message code="task.startMoment"/></b>: <fmt:formatDate value="${task.startMoment}" pattern="${datePattern}" />
	<br/>
	<b><spring:message code="task.endMoment"/></b>: <fmt:formatDate value="${task.endMoment}" pattern="${datePattern}" />
	<br/>
	<b><spring:message code="task.warranty"/></b>: <jstl:out value="${task.warranty.title}"/>
	<br/>
	<jstl:if test="${pageContext.response.locale == 'en'}">
		<b><spring:message code="task.category"/></b>: <jstl:out value="${task.category.name}"/>
		<br/>
	</jstl:if>
	<jstl:if test="${pageContext.response.locale == 'es'}">
		<b><spring:message code="task.category"/></b>: <jstl:out value="${task.category.nombre}"/>
		<br/>
	</jstl:if>
	<b><spring:message code="task.phases"/></b>: <jstl:out value="${task.phases}"/>
	<br/>
	<security:authorize access="hasRole('HANDYWORKER')">
		<br> <spring:message code="task.complaints" />:
	<a href="complaint/handyWorker/list.do?taskId=${taskId}">
			<spring:message code="task.complaints"/>
		</a>
		</security:authorize>
		
	<security:authorize access="hasRole('REFEREE')">
		<br> <spring:message code="task.complaints" />:
	<a href="complaint/referee/list.do?taskId=${taskId}">
			<spring:message code="task.complaints"/>
		</a>
		</security:authorize>	
		
		<security:authorize access="hasRole('CUSTOMER')">
		<br> <spring:message code="task.complaints" />:
	<a href="complaint/customer/listByTask.do?taskId=${taskId}">
			<spring:message code="task.complaints"/>
		</a>
		</security:authorize>
		
		
		<security:authorize access="hasRole('CUSTOMER')">
	<div>
		<a href="complaint/customer/create.do?taskId=${taskId}">
			<spring:message code="complaint.create"/>
		</a>
	</div>
</security:authorize>
		
	<br/>
	<b><spring:message code="task.applications"/></b>: <jstl:out value="${task.applications}"/>
	<br/>

	<!--Back-->

	<input type="button" name="cancel" onclick="javascript: window.location.replace('task/list.do')"
		value="<spring:message code="task.back" />" />

	<security:authorize access="hasRole('HANDYWORKER')">
        <jstl:if test="${canCreatePhase}">
        <div>
            <a href="phase/handyWorker/create.do?taskId=${taskId}">
                <spring:message code="phase.create.task"/>
            </a>
        </div>
        </jstl:if>
	</security:authorize>

</html>
	
