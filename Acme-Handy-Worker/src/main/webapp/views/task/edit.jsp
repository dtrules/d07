<%--
 * create.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="task.edit" /></p>

<security:authorize access="hasRole('CUSTOMER')">

<form:form action="task/customer/edit.do" modelAttribute="task">

<!--Ocultos-->

    <form:hidden path="id" />
    <form:hidden path="version" />  
    <form:hidden path="ticker" /> 
    <form:hidden path="publishDate" />


	<!--Description-->
	<form:label path="description">
		<spring:message code="task.description"/>
    	</form:label>
	<form:input path="description"/>
    	<form:errors cssClass="error" path="description" />
    <br/>
    	


    <!--Address-->
	<form:label path="address">
		<spring:message code="task.address"/>
    	</form:label>
	<form:textarea path="address"/>
    	<form:errors cssClass="error" path="address" />
   <br/>
    	


    <!--MaxPrice-->
	<form:label path="maxPrice">
		<spring:message code="task.maxPrice"/>
    	</form:label>
	<form:textarea path="maxPrice"/>
    	<form:errors cssClass="error" path="maxPrice" />
    <br/>



     <!--StartMoment-->
 	<form:label path="startMoment">
 		<spring:message code="task.startMoment"/>
     	</form:label>
 	<form:textarea path="startMoment"/>
     	<form:errors cssClass="error" path="startMoment" />
    <br/>



     <!--EndMoment-->
 	<form:label path="endMoment">
 		<spring:message code="task.endMoment"/>
     	</form:label>
 	<form:textarea path="endMoment"/>
     	<form:errors cssClass="error" path="endMoment" />
    <br/>



     <!--Warranty-->
     <form:label path="warranty">
         <spring:message code="task.warranty"/>
         </form:label>
     <form:select id="warranties" path="warranty">
         <form:options items="${warranties}" itemLabel="title" itemValue="id"/>
         <form:option value="0" label="------"/>
     </form:select>
     <form:errors cssClass="error" path="warranty"/>
     <br/>


     <!--Category-->
     <form:label path="category">
         <spring:message code="task.category"/>
         </form:label>
     <form:select id="categories" path="category">
         <form:options items="${categories}" itemLabel="name" itemValue="id"/>
         <form:option value="0" label="------"/>
     </form:select>
     <form:errors cssClass="error" path="category"/>
     <br/>
	
	<input type="submit" name="save" value="<spring:message code="task.save"/>"/>

	<input type="button" name="cancel"
		value="<spring:message code="task.cancel" />"
		onclick="javascript: relativeRedir('task/list.do');" />

</form:form>
</security:authorize>
