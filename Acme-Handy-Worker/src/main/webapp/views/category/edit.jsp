<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="category.edit" /></p>

<security:authorize access="hasRole('ADMIN')">

<form:form action="category/administrator/edit.do" modelAttribute="category">

<!--Ocultos-->

    <form:hidden path="id" />
    <form:hidden path="version" />  
	

	<!--Name-->
	<form:label path="name">
		<spring:message code="category.name"/>
    	</form:label>
	<form:input path="name"/>
    	<form:errors cssClass="error" path="name" />    	
    <br/>

    <!--Nombre-->
	<form:label path="nombre">
		<spring:message code="category.nombre"/>
    	</form:label>
	<form:input path="nombre"/>
    	<form:errors cssClass="error" path="nombre" />    	
    <br/>
    
    <!--Parent Category-->
    <form:label path="parentCategory">
        <spring:message code="category.parentCategory"/>
        </form:label>
    <form:select id="categories" path="parentCategory">
    	<form:option value="0" label="------"/>
        <form:options items="${categories}" itemLabel="name" itemValue="id"/>        
    </form:select>
    <form:errors cssClass="error" path="parentCategory"/>
    <br/>

	
	<input type="submit" name="save" value="<spring:message code="category.save"/>"/>
	<input type="button" name="cancel"
		value="<spring:message code="category.cancel" />"
		onclick="javascript: relativeRedir('category/administrator/list.do');" />
	<br/>

</form:form>
</security:authorize>
