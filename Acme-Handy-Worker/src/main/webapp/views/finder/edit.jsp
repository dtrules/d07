<%--
 * edit.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="finder.edit" /></p>

<security:authorize access="hasRole{'HANDYWORKER'}">

	<form:form action="finder/edit.do" modelAttribute="finder">
	
		<!--Ocultos-->
	    <form:hidden path="id" />
	    <form:hidden path="version" />
	    <form:hidden path="moment" />
	    <form:hidden path="tasks" />
	    
	    <!--Key Word-->
		<form:label path="keyWord">
			<spring:message code="finder.keyWord"/>
	    </form:label>
		<form:input path="keyWord"/>
	    	<form:errors cssClass="error" path="keyWord" />
	    	
	    <!--Min Price-->
		<form:label path="minPrice">
			<spring:message code="finder.minPrice"/>
	    	</form:label>
		<form:input path="minPrice"/>
	    	<form:errors cssClass="error" path="minPrice" />
	    
	    <!--Max Price-->
		<form:label path="maxPrice">
			<spring:message code="finder.maxPrice"/>
	    	</form:label>
		<form:input path="maxPrice"/>
	    	<form:errors cssClass="error" path="maxPrice" />
	    	
	    <!--Start moment-->
	    <form:label path="startDate">
			<spring:message code="finder.startMoment"/>
	    	</form:label>
		<form:input path="startDate"/>
	    	<form:errors cssClass="error" path="startDate" />
	    	
	    <!--End moment-->
	    <form:label path="endDate">
			<spring:message code="finder.endDate"/>
	    	</form:label>
		<form:input path="endDate"/>
	    	<form:errors cssClass="error" path="endDate" />
	    	
	    <!--Category-->
		<form:label path="category">
			<spring:message code="finder.category"/>
	    	</form:label>
		<form:input path="category"/>
	    	<form:errors cssClass="error" path="category" />
	    	
	    <!--Warranty-->
		<form:label path="warranty">
			<spring:message code="finder.warranty"/>
	    	</form:label>
		<form:input path="warranty"/>
	    	<form:errors cssClass="error" path="warranty" />
	    	
	    <br/>	
		<input type="submit" name="save" value="<spring:message code="finder.save"/>"/>
		<input type="submit" name="cancel" value="<spring:message code="finder.cancel"/>"/>
		
		<input type="button" name="cancel"
		value="<spring:message code="finder.cancel" />"
		onclick="javascript: relativeRedir('finder/handyWorker/display.do');" />
		
		<br/>
	
	</form:form>
</security:authorize>