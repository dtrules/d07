<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
 
<security:authorize access="hasRole('REFEREE')">
<form:form action="referee/referee/edit.do" modelAttribute="referee" onsubmit="return checkPhoneNumber()">
	<form:hidden path="id"/>
	<form:hidden path="version"/>
	<form:hidden path="userAccount.authorities"/>
	<form:hidden path="isSuspicious"/>
	<form:hidden path="isBanned"/>
	<form:hidden path="profiles"/>
	<form:hidden path="folders"/>
	<form:hidden path="reports"/>
	
	

	<form:label path="userAccount.username">
		<spring:message code="actor.username" />:
	</form:label>
	<form:input path="userAccount.username"/>
	<form:errors cssClass="error" path="userAccount.username" />
	
	<br/>
	
	<form:label path="userAccount.password">
		<spring:message code="actor.password" />:
	</form:label>
	<form:password path="userAccount.password"/>
	<form:errors cssClass="error" path="userAccount.password" />
	
	<br/>
	
	<form:label path="name">
		<spring:message code="actor.name" />:
	</form:label>
	<form:input path="name"/>
	<form:errors cssClass="error" path="name" />		
	
	<br/>
	
	<form:label path="middleName">
		<spring:message code="actor.middleName" />:
	</form:label>
	<form:input path="middleName"/>
	<form:errors cssClass="error" path="middleName" />		
	
	<br/>

	<form:label path="surname">
		<spring:message code="actor.surname" />:
	</form:label>
	<form:input path="surname"/>
	<form:errors cssClass="error" path="surname" />		
	
	<br/>
	
	
	<form:label path="photo">
		<spring:message code="actor.photo" />:
	</form:label>
	<form:input path="photo"/>
	<form:errors cssClass="error" path="photo" />	
	
	<br/>
	
	
	<form:label path="email">
		<spring:message code="actor.email" />:
	</form:label>
	<form:input path="email"/>
	<form:errors cssClass="error" path="email" />	
	
	<br/>
	
	<form:label path="phoneNumber">
	<spring:message code="actor.phoneNumber" />:
	</form:label>
	<form:input id="phoneNumber" path="phoneNumber"/>
	<form:errors cssClass="error" path="phoneNumber" />
	
	<br/>
	
	<form:label path="address">
	<spring:message code="actor.address" />:
	</form:label>
	<form:input path="address"/>
	<form:errors cssClass="error" path="address" />

	<br/>

	<input type="submit" name="save" value="<spring:message code="actor.edit.save" />" />
	<input type="button" name="cancel" onclick="javascript: window.location.replace('welcome/index.do')"
			value="<spring:message code="actor.edit.cancel" />" />

	
</form:form>
</security:authorize>

<script>
console.log("loaded");
function checkPhoneNumber(){
	console.log("checking...");
	var elem = document.getElementById("phoneNumber");

    var re = /^(\+[1-9][0-9]*(\([0-9]*\)|-[0-9]*-))?[0]?[1-9][0-9\- ]*$/;

    if(!re.test(elem.value)){
    	console.log("no");
    	return confirm("Is this your phone number? \n" + elem.value);
    }
    else{
    	console.log("yes");
    	return true;
    }
}

</script>