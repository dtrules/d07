<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<!--Tabla-->

<display:table name="messages" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de borrar en cada fila-->

	<security:authorize access="isAuthenticated()">
	<display:column>
		<a href="message/actor/delete.do?messageId=${row.id}">
			<spring:message code="message.delete"/>
		</a>
	</display:column>
	
	<display:column>
		<a href="message/actor/display.do?messageId=${row.id}">
			<spring:message code="message.display"/>
		</a>
	</display:column>
	<display:column>
		<form action="message/actor/move.do" method="GET">
			<input type="hidden" id="messageId" name="messageId" value="${row.id}" />
			<select id="folderId" name=folderId>
				<option label="----" value="0"/>
				<jstl:forEach items="${folders}" var="folder" >
					<option label="<jstl:out value="${folder.name}" />" value="${folder.id}" />
				</jstl:forEach>
			</select>
			<spring:message code="message.move" var="moveTitle" />
			<input type="submit" value="${moveTitle}" />
		</form>
	</display:column>
	</security:authorize>
	
	<display:column property="subject" titleKey="message.subject" sortable="true"/>
	<display:column property="body" titleKey="message.body" sortable="true"/>
	<display:column property="priority.value" titleKey="message.priority" sortable="true"/>
	<display:column property="moment" titleKey="message.moment" sortable="true"/>
</display:table>



<!--Bot�n de crear debajo de la lista-->

<security:authorize access="isAuthenticated()">
	<div>
		<a href="message/actor/create.do">
			<spring:message code="message.create"/>
		</a>
	</div>
</security:authorize>
