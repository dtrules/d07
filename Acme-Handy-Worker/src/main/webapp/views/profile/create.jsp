<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p>
	<spring:message code="profile.create" />
</p>

<security:authorize access="isAuthenticated()">

	<form:form action="profile/actor/create.do" modelAttribute="profile">

		<!--Ocultos-->

		<form:hidden path="id" />
		<form:hidden path="version" />

		<!--Nick-->
		<form:label path="nick">
			<spring:message code="profile.nick" />
		</form:label>
		<form:input path="nick" />
		<form:errors cssClass="error" path="nick" />

		<!--SocialNetwork-->
		<form:label path="socialNetwork">
			<spring:message code="profile.socialNetwork" />
		</form:label>
		<form:input path="socialNetwork" />
		<form:errors cssClass="error" path="socialNetwork" />

		<!--Link-->
		<form:label path="link">
			<spring:message code="profile.link" />
		</form:label>
		<form:input path="link" />
		<form:errors cssClass="error" path="link" />

	<input type="submit" name="save" value="<spring:message code="profile.save"/>"/>

	<input type="button" name="cancel"
		value="<spring:message code="profile.cancel" />"
		onclick="javascript: relativeRedir('profile/actor/list.do');" />

	</form:form>
</security:authorize>