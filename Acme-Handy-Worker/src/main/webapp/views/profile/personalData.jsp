<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p>
	<spring:message code="profile.personalData" />
</p>

<security:authorize access="isAuthenticated()">

	<form:form action="profile/actor/personalData.do" modelAttribute="actor" onsubmit="return checkPhoneNumber()">

		<!--Ocultos-->

		<form:hidden path="id" />
		<form:hidden path="version" />
		<form:hidden path="profiles" />
		<form:hidden path="folders" />
		<form:hidden path="userAccount" />
		<form:hidden path="isBanned" />
		<form:hidden path="isSuspicious" />

		<!--Name-->
		<form:label path="name">
			<spring:message code="actor.name" />
		</form:label>
		<form:input path="name" />
		<form:errors cssClass="error" path="name" />
		<br/>
		
		<!--MiddleName-->
		<form:label path="middleName">
			<spring:message code="actor.middleName" />
		</form:label>
		<form:input path="middleName" />
		<form:errors cssClass="error" path="middleName" />
		<br/>

		<!--Surname-->
		<form:label path="surname">
			<spring:message code="actor.surname" />
		</form:label>
		<form:input path="surname" />
		<form:errors cssClass="error" path="surname" />
		<br/>

		<!--Photo-->
		<form:label path="photo">
			<spring:message code="actor.photo" />
		</form:label>
		<form:input path="photo" />
		<form:errors cssClass="error" path="photo" />
		<br/>
		
		<!--PhoneNumber-->
		<form:label path="phoneNumber">
			<spring:message code="actor.phoneNumber" />
		</form:label>
		<form:input id="phoneNumber" path="phoneNumber" />
		<form:errors cssClass="error" path="phoneNumber" />
		<br/>		
		
		<!--Address-->
		<form:label path="address">
			<spring:message code="actor.address" />
		</form:label>
		<form:input path="address" />
		<form:errors cssClass="error" path="address" />
		<br/>
		
		<!--Address-->
		<form:label path="email">
			<spring:message code="actor.email" />
		</form:label>
		<form:input path="email" />
		<form:errors cssClass="error" path="email" />
		<br/>				
						

	<input type="submit" name="save" value="<spring:message code="actor.save"/>"/>

	<input type="button" name="cancel"
		value="<spring:message code="actor.cancel" />"
		onclick="javascript: relativeRedir('welcome.do');" />

	</form:form>
</security:authorize>

<script>
console.log("loaded");
function checkPhoneNumber(){
	console.log("checking...");
	var elem = document.getElementById("phoneNumber");

    var re = /^(\+[1-9][0-9]*(\([0-9]*\)|-[0-9]*-))?[0]?[1-9][0-9\- ]*$/;

    if(!re.test(elem.value)){
    	console.log("no");
    	return confirm("Is this your phone number? \n" + elem.value);
    }
    else{
    	console.log("yes");
    	return true;
    }
}

</script>