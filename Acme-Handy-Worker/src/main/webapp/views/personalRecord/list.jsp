<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!-- Listing grid -->
	<security:authorize access="hasRole('HANDYWORKER')">
<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="personalRecords" requestURI="${requestURI}" id="row">
	
	<!-- Action links -->

	<%-- <security:authorize access="hasRole('ADMIN')">
		<display:column>
			<a href="announcement/administrator/edit.do?announcementId=${row.id}">
				<spring:message	code="announcement.edit" />
			</a>
		</display:column>		
	</security:authorize> --%>

	
	<!-- Attributes -->
	
	<spring:message code="personalRecord.name" var="nameHeader" />
	<display:column property="name" title="${nameHeader}" sortable="true" />

	<spring:message code="personalRecord.photo" var="photoHeader" />
	<display:column property="photo" title="${photoHeader}" sortable="true" />

	<spring:message code="personalRecord.email" var="emailHeader" />
	<display:column property="email" title="${emailHeader}" sortable="true" />
	
	<spring:message code="personalRecord.phoneNumber" var="phoneNumberHeader" />
	<display:column property="phoneNumber" title="${phoneNumberHeader}" sortable="true" />

	<spring:message code="personalRecord.linkedinProfile" var="linkedinProfileHeader" />
	<display:column property="linkedinProfile" title="${linkedinProfileHeader}" sortable="true" />

</display:table>
</security:authorize>

<!-- Action links -->

<security:authorize access="hasRole('HANDYWORKER')">
	<div>
		<a href="personalRecord/handyWorker/create.do"> <spring:message
				code="personalRecord.create" />
		</a>
	</div>
</security:authorize>