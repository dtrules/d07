<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="actor.list"/></p>

<!--Tabla-->

<display:table name="actors" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de editar y ver en cada fila-->

	<display:column>
		<a href="actor/edit.do?actorId=${row.id}">
			<spring:message code="actor.edit"/>
		</a>
	</display:column>
	
	<display:column>
		<a href="actor/display.do?actorId=${row.id}">
			<spring:message code="actor.display"/>
		</a>
	</display:column>
	
	<spring:message code="actor.name" var="nameHeader"/>
	<display:column property="name" title="${nameHeader}" sortable="true"/>
	
	<spring:message code="actor.middleName" var="middleNameHeader"/>
	<display:column property="middleName" title="${middleNameHeader}" sortable="true"/>
	
	<spring:message code="actor.surname" var="surnameHeader"/>
	<display:column property="surname" title="${surnameHeader}" sortable="true"/>
	
	<spring:message code="actor.photo" var="photoHeader"/>
	<display:column property="photo" title="${photoHeader}" sortable="true"/>
	
	<spring:message code="actor.email" var="emailHeader"/>
	<display:column property="email" title="${emailHeader}" sortable="true"/>
	
	<spring:message code="actor.phoneNumber" var="phoneNumberHeader"/>
	<display:column property="phoneNumber" title="${phoneNumberHeader}" sortable="true"/>
	
	<spring:message code="actor.address" var="addressHeader"/>
	<display:column property="address" title="${addressHeader}" sortable="true"/>
	
	<security:authorize access="hasRole('ADMIN')">
	<spring:message code="actor.suspicious" var="suspiciousHeader"/>
	<display:column property="nick" title="${nickHeader}" sortable="true"/>
	
	<spring:message code="actor.banned" var="bannedHeader"/>
	<display:column property="banned" title="${bannedHeader}" sortable="true"/>
	</security:authorize>
	
</display:table>

<!--Bot�n de crear debajo de la lista-->

	<div>
		<a href="actor/create.do">
			<spring:message code="actor.create"/>
		</a>
	</div>