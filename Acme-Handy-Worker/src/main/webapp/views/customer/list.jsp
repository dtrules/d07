<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:customer code="customer.list" /></p>

<!--Tabla-->

<display:table name="customers" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de editar en cada fila-->

	<security:authorize access="hasRole('ADMIN')">
	<display:column>
		<a href="customer/edit.do?customerId=${row.id}">
			<spring:customer code="customer.edit"/>
		</a>
	</display:column>
	
	<display:column>
		<a href="customer/display.do?customerId=${row.id}">
			<spring:customer code="customer.display"/>
		</a>
	</display:column>
	</security:authorize>
	<display:column property="name" titleKey="customer.name" sortable="true"/>
	<display:column property="middleName" titleKey="customer.middleName" sortable="true"/>
	<display:column property="surname" titleKey="customer.surname" sortable="true"/>
	<display:column property="photo" titleKey="customer.photo" sortable="true"/>
	<display:column property="email" titleKey="customer.email" sortable="true"/>
	<display:column property="phoneNumber" titleKey="customer.phoneNumber" sortable="true"/>
	<display:column property="address" titleKey="customer.address" sortable="true"/>
	<display:column property="username" titleKey="customer.username" sortable="true"/>
	<display:column property="password" titleKey="customer.password" sortable="true"/>
</display:table>

<!--Bot�n de crear debajo de la lista-->

	<security:authorize access="isAuthenticated()">
	<div>
		<a href="customer/create.do">
			<spring:customer code="customer.create"/>
		</a>
	</div>
	</security:authorize>