<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<security:authorize access="hasRole('CUSTOMER')">

<form:form action="application/customer/creditCard.do" modelAttribute="application" >

 <form:hidden path="id" />
    <form:hidden path="version" />  
	<form:hidden path="moment" />  
	<form:hidden path="price" />
	<form:hidden path="task" />
		
		
		

		
	        <!--Status-->
    <form:label path="status">
        <spring:message code="application.status"/>
        </form:label>
    <form:select id="status" path="status">
        <option value="${'ACCEPTED'}">
				<spring:message code="application.status.accepted"/>
			</option>
			<option value="${'REJECTED'}">
				<spring:message code="application.status.rejected"/>
			</option>
    </form:select>
    <form:errors cssClass="error" path="status"/>
    <br/>	
    
    	
    	
   
    	
	        	
    	<!--CreditCard-->
     <form:label path="creditCard">
         <spring:message code="application.creditCard"/>
         </form:label>
     <form:select id="creditCards" path="creditCard">
      	 <form:option value="0" label="------"/>
         <form:options items="${creditCards}" itemLabel="number" itemValue="id"/>
        
     </form:select>
     <form:errors cssClass="error" path="creditCard"/>     
     <br/>

	

			<!-- Comments-->
    	<form:label path="comments">
		<spring:message code="application.comments"/>
    	</form:label>
    	<form:input path="comments" />
    	<form:errors cssClass="error" path="comments" />
	<br/>
	

	<!--  Botones de save y cancelar -->
	
	<input type="submit" name="save" value="<spring:message code="application.save"/>"/>
	
	<input type="button" name="cancel"
		value="<spring:message code="application.cancel" />"
		onclick="javascript: relativeRedir('application/list.do');" />
	
	

</form:form>
</security:authorize>
