<%--
 * action-2.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<!--Tabla-->

<jsp:useBean id="now" class="java.util.Date"/> 

<display:table name="applications" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

	<!-- La lista con el bot�n de editar en cada fila -->



<jstl:choose>
		
		<jstl:when test="${row.status.value eq 'REJECTED'}">
			<jstl:set var = "statusColor" value = "#EF7F1A" />
		</jstl:when>
		<jstl:when test="${row.status.value eq 'ACCEPTED'}">
			<jstl:set var = "statusColor" value = "#009846" />
		</jstl:when>
		
		<jstl:when test="${row.task.endMoment lt now and row.status.value eq 'PENDING'}">
			<jstl:set var = "statusColor" value = "#838383" />
		</jstl:when>

</jstl:choose>
<display:column>
 <a href="application/display.do?applicationId=${row.id}">
        <spring:message code="application.display"/>
      </a>
	    </display:column>
	
	<security:authorize access="hasRole('HANDYWORKER')">
	
    <display:column>
    <jstl:if test="${row.status.value eq 'PENDING'}">
      <a href="application/handyWorker/edit.do?applicationId=${row.id}">
        <spring:message code="application.edit"/>
      </a>
          		</jstl:if>
      
    </display:column>
      </security:authorize>
    
    <security:authorize access="hasRole('CUSTOMER')">
	
    <display:column>
    <jstl:if test="${row.status.value eq 'PENDING'}">
      <a href="application/customer/creditCard.do?applicationId=${row.id}">
        <spring:message code="application.edit"/>
      </a>
          		</jstl:if>
      
    </display:column>
      </security:authorize>
    
    
  
    
	
	<spring:message code="application.moment" var="momentHeader"/>
	<display:column style="background-color: ${statusColor};" property="moment" title="${momentHeader}" sortable="true" />
	
	<spring:message code="application.status" var="statusHeader"/>
	<display:column style="background-color: ${statusColor};" property="status.value" title="${statusHeader}" sortable="true" />
	
	<spring:message code="application.price" var="priceHeader"/>
	<display:column style="background-color: ${statusColor};" property="price" title="${priceHeader}" sortable="true" />
	
	<spring:message code="application.comments" var="commentsHeader"/>
	<display:column style="background-color: ${statusColor};" property="comments" title="${commentsHeader}" sortable="true" />
	
	<spring:message code="application.task" var="taskHeader"/>
	<display:column style="background-color: ${statusColor};" property="task.ticker" title="${taskHeader}" sortable="true" />		
	
	
</display:table>


	<security:authorize access="hasRole('HANDYWORKER')">
  <div>
    <a href="application/handyWorker/create.do">
      <spring:message code="application.create"/>
    </a>
  </div>
</security:authorize>









