<%--
 * header.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
	<a href="#"><img src="https://i.imgur.com/WIb1Z1Q.jpg" alt="Acme Handy Worker Co., Inc." /></a>
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize access="hasRole('ADMIN')">
			<li><a class="fNiv"><spring:message	code="master.page.administrator" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="administrator/create.do"><spring:message code="master.page.administrator.create" /></a></li>
					<li><a href="dashboard/administrator/list.do"><spring:message code="master.page.administrator.dashboard" /></a></li>
					<li><a href="customization/administrator/edit.do"><spring:message code="master.page.administrator.customization" /></a></li>
					<li><a href="administrator/suspicious.do"><spring:message code="master.page.administrator.suspicious" /></a></li>				
				</ul>
			</li>
            <li><a class="fNiv"><spring:message	code="master.page.category" /></a>
                <ul>
                    <li class="arrow"></li>
                        <li><a href="category/administrator/list.do"><spring:message code="master.page.category.list" /></a></li>
                </ul>
            </li>
            <li><a class="fNiv"><spring:message	code="master.page.warranty" /></a>
                <ul>
                    <li class="arrow"></li>
                        <li><a href="warranty/administrator/list.do"><spring:message code="master.page.warranty.list" /></a></li>
                </ul>
            </li>
            <li><a class="fNiv"><spring:message	code="master.page.personal.admin" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="administrator/edit.do"><spring:message code="master.page.personal.admin" /></a></li>
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('CUSTOMER')">
			<li><a class="fNiv"><spring:message	code="master.page.application" /></a>
				<ul>
					<li class="arrow"></li>
						<li><a href="application/list.do"><spring:message code="master.page.application.list" /></a></li>
				</ul>
			</li>
			<li><a class="fNiv"><spring:message	code="master.page.creditCard" /></a>
				<ul>
					<li class="arrow"></li>
						<li><a href="creditCard/customer/create.do"><spring:message code="master.page.creditCard.create" /></a></li>
				</ul>
			</li>
			<li><a class="fNiv"><spring:message	code="master.page.personal.customer" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="customer/edit.do"><spring:message code="master.page.personal.customer" /></a></li>
				</ul>
			</li>
			<li><a class="fNiv"><spring:message	code="master.page.task" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="task/list.do"><spring:message code="master.page.task.list" /></a></li>
					<li><a href="task/customer/create.do"><spring:message code="master.page.task.create" /></a></li>
				</ul>
			</li>
			<li><a class="fNiv"><spring:message	code="master.page.complaint" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="complaint/customer/list.do"><spring:message code="master.page.complaint.list" /></a></li>
				</ul>
			</li>
			
				<li><a class="fNiv"><spring:message	code="master.page.report" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="report/list.do"><spring:message code="master.page.report.list" /></a></li>
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('HANDYWORKER')">
			<li><a class="fNiv"><spring:message	code="master.page.handyWorker" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="finder/handyWorker/display.do"><spring:message code="master.page.handyWorker.finder" /></a></li>
					<li><a href="application/list.do"><spring:message code="master.page.handyWorker.application.list" /></a></li>
					<li><a href="application/handyWorker/create.do"><spring:message code="master.page.handyWorker.application.create" /></a></li>
					
				</ul>
			</li>
			<li><a class="fNiv"><spring:message	code="master.page.personal.handyWorker" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="handyWorker/edit.do"><spring:message code="master.page.handyWorker.edit" /></a></li>
				</ul>
			</li>
			<li><a class="fNiv"><spring:message	code="master.page.application" /></a>
				<ul>
					<li class="arrow"></li>
						<li><a href="application/list.do"><spring:message code="master.page.application.list" /></a></li>
				</ul>
			</li>						
			<li><a href="curriculum/handyWorker/list.do"><spring:message	code="master.page.curriculum" /></a>
				
			</li>
			<li><a class="fNiv"><spring:message	code="master.page.task" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="task/list.do"><spring:message code="master.page.task.list" /></a></li>
				</ul>
			</li>
            <li><a class="fNiv"><spring:message	code="master.page.phase" /></a>
                <ul>
                    <li class="arrow"></li>
                    <li><a href="phase/handyWorker/list.do"><spring:message code="master.page.phase.list" /></a></li>
                </ul>
            </li>
			<li><a class="fNiv"><spring:message	code="master.page.report" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="report/list.do"><spring:message code="master.page.report.list" /></a></li>
				</ul>
			</li>
		</security:authorize>
		
		
		<security:authorize access="hasRole('SPONSOR')">
			<li><a class="fNiv"><spring:message	code="master.page.sponsor" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="sponsorship/sponsor/create.do"><spring:message code="master.page.sponsor.sponsorship" /></a></li>
					
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('REFEREE')">
			<li><a class="fNiv"><spring:message	code="master.page.referee" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="report/referee/list.do"><spring:message code="master.page.referee.reports" /></a></li>
					<li><a href="complaint/referee/list.do"><spring:message code="master.page.referee.complaints" /></a></li>
					<li><a href="complaint/referee/listAssigned.do"><spring:message code="master.page.referee.complaintsAssigned" /></a></li>
					
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a></li>
			<li>
			<a class="fNiv"><spring:message	code="master.page.register" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="customer/create.do"><spring:message code="master.page.customer.create" /></a></li>
					<li><a href="handyWorker/create.do"><spring:message code="master.page.handyWorker.create" /></a></li>
					<li><a href="sponsor/create.do"><spring:message code="master.page.sponsor.create" /></a></li>
				</ul>
			</li>
			</security:authorize>
		
		<security:authorize access="isAuthenticated()">
			<li><a class="fNiv"><spring:message	code="master.page.profile" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="profile/actor/list.do"><spring:message code="master.page.profile.list" /></a></li>
				</ul>
			</li>
			<li>
			<a class="fNiv"><spring:message	code="master.page.message" /></a>
				<ul>
					<li class="arrow"></li>
					<li><a href="folder/actor/listSystem.do"><spring:message code="master.page.folder.listSystem" /></a></li>
					<li><a href="folder/actor/list.do"><spring:message code="master.page.folder.list" /></a></li>
					<li><a href="folder/actor/create.do"><spring:message code="master.page.folder.create" /></a></li>
					<li><a href="message/actor/create.do"><spring:message code="master.page.message.create" /></a></li>
					<security:authorize access="hasRole('ADMIN')">
						<li><a href="message/actor/broadcast.do"><spring:message code="master.page.broadcast" /></a></li>
					</security:authorize>
					<li><a href="complaint/list.do"><spring:message code="master.page.complaint.list" /></a></li>
					<li><a href="tutorial/list.do"><spring:message code="master.page.tutorial.list" /></a></li>
				</ul>
			</li>
			<li><a class="fNiv" href="j_spring_security_logout"><spring:message code="master.page.logout" /></a></li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

