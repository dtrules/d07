<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<!-- Listing grid -->

<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="educationRecord" requestURI="${requestURI}" id="row">
	
	<!-- Action links -->

	
	<security:authorize access="hasRole('HANDYWORKER')">
		<display:column>
			<a href="educationRecord/handyWorker/edit.do?educationRecordId=${row.id}">
				<spring:message	code="educationRecord.edit" />
			</a>
		</display:column>		
	</security:authorize>

	
	<!-- Attributes -->
	
	<spring:message code="educationRecord.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />
	
	<spring:message code="educationRecord.startMoment" var="startMomentHeader" />
	<display:column property="startMoment" title="${startMomentHeader}" sortable="true" format="{0,date,dd/mm/yyyy HH:mm}"/>
	
	<spring:message code="educationRecord.endMoment" var="endMomentHeader" />
	<display:column property="endMoment" title="${endMomentHeader}" sortable="true" format="{0,date,dd/mm/yyyy HH:mm}"/>

	<spring:message code="educationRecord.institution" var="institutionHeader" />
	<display:column property="institution" title="${institutionHeader}" sortable="true" />
	
	<spring:message code="educationRecord.attachment" var="attachmentHeader" />
	<display:column property="attachment" title="${attachmentHeader}" sortable="true" />

</display:table>


<!-- Action links -->

<security:authorize access="hasRole('HANDYWORKER')">
	<div>
		<a href="educationRecord/handyWorker/create.do"> <spring:message
				code="educationRecord.create" />
		</a>
	</div>
</security:authorize>