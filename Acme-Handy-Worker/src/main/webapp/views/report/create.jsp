<%--
 * create.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<security:authorize access="hasRole('REFEREE')">

<form:form action="report/referee/create.do?complaintId=${complaintId}" modelAttribute="report">

<!--Ocultos-->

    <form:hidden path="id" />
    <form:hidden path="version" />  
	<form:hidden path="moment" /> 
	

	<!--Description-->
	<form:label path="description">
		<spring:message code="report.description"/>
    	</form:label>
	<form:input path="description"/>
    	<form:errors cssClass="error" path="description" />
    	
    	<br/>
    <!--Attachments-->
	<form:label path="attachment">
		<spring:message code="report.attachments"/>
    	</form:label>
	<form:textarea path="attachment"/>
    	<form:errors cssClass="error" path="attachment" />
    	
    	<br/>
    	
    	<!--DraftMode-->
	<form:label path="draftMode">
		<spring:message code="report.draftMode"/>
    	</form:label>
	<form:checkbox path="draftMode"/>
    	<form:errors cssClass="error" path="draftMode" />
    	
    	<br/>
   
    	
	
	<input type="submit" name="save" value="<spring:message code="report.save"/>"/>
	
	
	<input type="button" name="cancel"
		value="<spring:message code="report.cancel" />"
		onclick="javascript: relativeRedir('task/list.do');" />

</form:form>
</security:authorize>
