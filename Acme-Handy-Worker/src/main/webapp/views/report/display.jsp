<%--
 * display.jsp
 *
 * Copyright (C) 2015 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>




<div style="position: relative; width: 500px; height: 250px;">



	<br> <spring:message code="report.moment" />:
	
	<jstl:out value="${report.moment}" />
	
	<br> <spring:message code="report.description" />:
	
	<jstl:out value="${report.description}" />
	
	<br> <spring:message code="report.attachments" />:
	
	<jstl:out value="${report.attachment}" />
	
	
	<br> <spring:message code="report.complaint" />:
	
	<jstl:out value="${report.complaint.ticker}" />
	
	
	
	<br>
	<input type="button" name="cancel"
		value="<spring:message code="report.back" />"
		onclick="javascript: relativeRedir('report/list.do');" />	
	
</div>


</html>