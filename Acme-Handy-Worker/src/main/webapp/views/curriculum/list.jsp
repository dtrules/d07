<%--
 * action-1.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!-- Listing grid -->
	<security:authorize access="hasRole('HANDYWORKER')">

	
	<!-- Attributes -->
	
	
<jstl:if test = "${haveCurriculum == false}">
	<a href="curriculum/handyWorker/create.do"> <spring:message
				code="curriculum.create" /> </a>
</jstl:if>


<jstl:if test = "${haveCurriculum == true}">
	<spring:message code="curriculum.ticker" />:	
		<jstl:out value="${ticker}" />
	<ul>
	<li><a href="personalRecord/handyWorker/list.do"><spring:message code="master.page.handyWorker.personalRecord.list" /></a></li>
	<li><a href="professionalRecord/handyWorker/list.do"><spring:message code="master.page.handyWorker.professionalRecord.list" /></a></li>
	<li><a href="educationRecord/handyWorker/list.do"><spring:message code="master.page.handyWorker.educationRecord.list" /></a></li>	
	<li><a href="miscellaneousRecord/handyWorker/list.do"><spring:message code="master.page.handyWorker.miscellaneousRecord.list" /></a></li>
	<li><a href="endorserRecord/handyWorker/list.do"><spring:message code="master.page.handyWorker.endorserRecord.list" /></a></li>
	</ul>
</jstl:if>

</security:authorize>


