<%--
 * create.jsp
 *
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<security:authorize access="hasRole('HANDYWORKER')">

<form:form action="section/handyWorker/create.do?tutorialId=${tutorialId}" modelAttribute="section">

<!--Ocultos-->

    <form:hidden path="id" />
    <form:hidden path="version" />  
	
    	
    	<!--Title-->
	<form:label path="title">
		<spring:message code="section.title"/>
    	</form:label>
	<form:input path="title"/>
    	<form:errors cssClass="error" path="title" />
    	
    </br>	
    	 <!--Text-->
	<form:label path="text">
		<spring:message code="section.text"/>
    	</form:label>
	<form:textarea path="text"/>
    	<form:errors cssClass="error" path="text" />
    	</br>
    	
     <!--Pictures-->
	<form:label path="picture">
		<spring:message code="section.picture"/>
    	</form:label>
	<form:textarea path="picture"/>
    	<form:errors cssClass="error" path="picture" />
    	
    	</br>
    	<!-- Number-->
    	<form:label path="number">
		<spring:message code="section.number"/>
    	</form:label>
    	<form:input path="number" />
    	<form:errors cssClass="error" path="number" />
	</br>
	<input type="submit" name="save"
		value="<spring:message code="section.save" />" 
		onclick="return confirm('<spring:message code="section.confirm.save" />')" />&nbsp;
		
		
	<input type="button" name="cancel"
		value="<spring:message code="section.cancel" />"
		onclick="javascript: relativeRedir('tutorial/list.do');" />
	<br />

</form:form>
</security:authorize>
