<%--
 * suspicious.jsp
 *
 * Copyright (C) 2019 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<p><spring:message code="administrator.suspicious" /></p>

<!--Tabla-->

<display:table name="suspiciousActors" id="row" requestURI="${requestURI}" pagesize="5" class="displaytag">

<!-- La lista con el bot�n de banear en cada fila-->

	<display:column titleKey="administrator.ban">
            <jstl:if test="${row.isBanned == false}">
                <a href="administrator/ban.do?actorUsername=${row.userAccount.username}">
                    <spring:message code="administrator.ban"/>
                </a>
            </jstl:if>
           </display:column>
           
	<display:column titleKey="administrator.unban">
            <jstl:if test="${row.isBanned == true}">
                <a href="administrator/unban.do?actorUsername=${row.userAccount.username}">
                    <spring:message code="administrator.unban"/>
                </a>
            </jstl:if>
    </display:column>
	
	<display:column property="name" titleKey="actor.name" sortable="true"/>
	<display:column property="middleName" titleKey="actor.middleName" sortable="true"/>
	<display:column property="surname" titleKey="actor.surname" sortable="true"/>
	<display:column property="userAccount.username" titleKey="actor.username" sortable="true"/>
	
</display:table>