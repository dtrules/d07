package controllers.referee;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import security.UserAccount;
import services.RefereeService;
import controllers.AbstractController;
import domain.Referee;


@Controller
@RequestMapping("/referee/referee")
public class RefereeRefereeController extends AbstractController{
	
	//Constructors ---------------------------------------
	
	public RefereeRefereeController(){
		super();
	}
	
	//Services -----------------------------
	
	@Autowired
	private RefereeService refereeService;
	
	
	
	

	// Edition ------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView result;
		Referee referee;

		final UserAccount userAccount = LoginService.getPrincipal();

		referee = this.refereeService.findRefereeByUserAccount(userAccount);
		Assert.notNull(referee);

		result = this.createEditModelAndView(referee);

		return result;

	}

	//Saving ---------------------------------------------------------------
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Referee referee, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = this.createEditModelAndView(referee);
		} else
			try {
				this.refereeService.save(referee);
				result = new ModelAndView("redirect:/welcome/index.do");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(referee, "actor.commit.error");
			}
		return result;
	}

	//Ancillary methods ----------------------------------------------------
	protected ModelAndView createEditModelAndView(final Referee referee) {
		ModelAndView result;

		result = this.createEditModelAndView(referee, null);

		return result;
	}

	private ModelAndView createEditModelAndView(final Referee referee, final String message) {
		ModelAndView result;

		result = new ModelAndView("referee/edit");
		result.addObject("referee", referee);
		result.addObject("message", message);

		return result;
	}

}
