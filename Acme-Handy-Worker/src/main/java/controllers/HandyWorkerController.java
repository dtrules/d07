
package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.HandyWorkerService;
import domain.HandyWorker;

@Controller
@RequestMapping("/handyWorker")
public class HandyWorkerController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public HandyWorkerController() {
		super();
	}


	// Services

	@Autowired
	HandyWorkerService	handyWorkerService;

	@Autowired
	ActorService		actorService;


	// Display ---------------------------------------------------

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView Display(@RequestParam final Integer handyWorkerId) {
		ModelAndView result;
		HandyWorker handyWorker;

		handyWorker = this.handyWorkerService.findOne(handyWorkerId);

		result = new ModelAndView("handyWorker/display");
		result.addObject("handyWorker", handyWorker);

		return result;
	}

	// Create ----------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final HandyWorker handyWorker = this.handyWorkerService.create();

		final ModelAndView result = this.createEditModelAndView(handyWorker);

		return result;
	}

	// Save del create -------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final HandyWorker handyWorker, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(handyWorker);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.handyWorkerService.save(handyWorker);

				res = new ModelAndView("redirect:/");

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createEditModelAndView(handyWorker, "handyWorker.commit.error");
			}

		return res;
	}

	// Edit ------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		final HandyWorker handyWorker = this.handyWorkerService.findByPrincipal();

		Assert.notNull(handyWorker);
		res = this.editModelAndView(handyWorker);

		return res;
	}

	// Save del Edit----------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final HandyWorker handyWorker, final BindingResult binding) {
		ModelAndView res;
		System.out.println(binding.getAllErrors());
		if (binding.hasErrors())
			res = this.editModelAndView(handyWorker);
		else
			try {
				this.handyWorkerService.save(handyWorker);
				res = new ModelAndView("redirect:/");

			} catch (final Throwable oops) {
				res = this.editModelAndView(handyWorker, "handyWorker.commit.error");
			}

		return res;
	}

	// List ----------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView res;
		Collection<HandyWorker> handyWorkers;

		handyWorkers = this.handyWorkerService.findAll();

		res = new ModelAndView("handyWorker/list");
		res.addObject("requestURI", "handyWorker/list.do");
		res.addObject("handyWorkers", handyWorkers);

		return res;
	}

	// Ancillary methods
	// ---------------------------------------------------------------

	private ModelAndView createEditModelAndView(final HandyWorker handyWorker) {

		return this.createEditModelAndView(handyWorker, null);
	}

	private ModelAndView createEditModelAndView(final HandyWorker handyWorker, final String message) {

		final ModelAndView res = new ModelAndView("handyWorker/create");
		res.addObject("handyWorker", handyWorker);
		res.addObject("message", message);

		return res;

	}

	private ModelAndView editModelAndView(final HandyWorker handyWorker) {

		return this.editModelAndView(handyWorker, null);
	}

	private ModelAndView editModelAndView(final HandyWorker handyWorker, final String message) {

		final ModelAndView res = new ModelAndView("handyWorker/edit");
		res.addObject("handyWorker", handyWorker);
		res.addObject("message", message);

		return res;

	}
}
