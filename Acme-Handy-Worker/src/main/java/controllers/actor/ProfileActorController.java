/*
 * ProfileController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.actor;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.Authority;
import services.ActorService;
import services.AdministratorService;
import services.CustomerService;
import services.HandyWorkerService;
import services.ProfileService;
import services.RefereeService;
import services.SponsorService;
import controllers.AbstractController;
import domain.Actor;
import domain.Administrator;
import domain.Customer;
import domain.HandyWorker;
import domain.Profile;
import domain.Referee;
import domain.Sponsor;

@Controller
@RequestMapping("/profile/actor")
public class ProfileActorController extends AbstractController {

	@Autowired
	private ActorService			actorService;

	@Autowired
	private AdministratorService	administratorService;

	@Autowired
	private HandyWorkerService		handyWorkerService;

	@Autowired
	private CustomerService			customerService;

	@Autowired
	private SponsorService			sponsorService;

	@Autowired
	private RefereeService			refereeService;

	@Autowired
	private ProfileService			profileService;


	//List-----------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Profile> profiles;

		profiles = this.actorService.findByPrincipal().getProfiles();

		result = new ModelAndView("profile/actor/list");
		result.addObject("requestURI", "profile/actor/list.do");
		result.addObject("profiles", profiles);

		return result;
	}

	// Display ---------------------------------------------------------------

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam final Integer profileId) {
		ModelAndView result;
		Profile profile;

		profile = this.profileService.findOne(profileId);

		result = new ModelAndView("profile/actor/display");
		result.addObject("profile", profile);

		return result;
	}

	// Create ---------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final Profile profile = this.profileService.create();

		final ModelAndView result = this.createEditModelAndView(profile);

		return result;
	}

	// Save del create ---------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Profile profile, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(profile);
		//System.out.println(binding.getAllErrors());
		else
			try {
				this.profileService.save(profile);
				result = new ModelAndView("redirect:/profile/actor/list.do");

			} catch (final Throwable oops) {
				//				System.out.println(oops);
				result = this.createEditModelAndView(profile, "profile.commit.error");
			}

		return result;
	}

	// Edit ---------------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int profileId) {
		ModelAndView result;
		Profile profile;

		profile = this.profileService.findOne(profileId);
		result = this.createEditModelAndView(profile);

		return result;
	}

	// Save del Edit ---------------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Profile profile, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(profile);
		//System.out.println(binding.getAllErrors());
		else
			try {
				this.profileService.save(profile);
				result = new ModelAndView("redirect:/profile/actor/list.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.createEditModelAndView(profile, "profile.commit.error");
			}

		return result;
	}
	// Delete ---------------------------------------------------------------

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int profileId) {
		ModelAndView result;

		final Profile profile = this.profileService.findOne(profileId);
		this.profileService.delete(profile);

		result = new ModelAndView("redirect:/profile/actor/list.do");

		return result;
	}

	// Ancillary methods ---------------------------------------------------------------

	private ModelAndView createEditModelAndView(final Profile profile) {

		return this.createEditModelAndView(profile, null);
	}

	private ModelAndView createEditModelAndView(final Profile profile, final String message) {

		final ModelAndView res = new ModelAndView("profile/actor/edit");
		res.addObject("profile", profile);
		res.addObject("message", message);

		return res;

	}

	// PersonalData ---------------------------------------------------------------

	@RequestMapping(value = "/personalData", method = RequestMethod.GET)
	public ModelAndView personalData() {

		final Actor a = this.actorService.findByPrincipal();
		System.out.println("llego aqui");
		final ModelAndView result = this.personalDataModelAndView(a);
		System.out.println("llego aqui2");

		return result;
	}

	@RequestMapping(value = "/personalData", method = RequestMethod.POST, params = "save")
	public ModelAndView savePersonalData(@Valid final Actor actor, final BindingResult binding) {
		ModelAndView result;
		System.out.println("llego antes del binding");
		if (binding.hasErrors()) {
			result = this.personalDataModelAndView(actor);
			System.out.println(binding.getAllErrors());
			System.out.println("llego despues del binding");
		} else
			try {
				System.out.println("llego al else");
				final Collection<Authority> authorities = this.actorService.findByPrincipal().getUserAccount().getAuthorities();
				if (authorities.contains(Authority.ADMIN)) {
					final Administrator a = this.administratorService.findOne(actor.getId());
					a.setAddress(actor.getAddress());
					a.setEmail(actor.getEmail());
					a.setName(actor.getName());
					a.setMiddleName(actor.getMiddleName());
					a.setPhoneNumber(actor.getPhoneNumber());
					a.setSurname(actor.getSurname());
					a.setPhoto(actor.getPhoto());
					this.administratorService.save(a);
				} else if (authorities.contains(Authority.CUSTOMER)) {
					final Customer a = this.customerService.findOne(actor.getId());
					a.setAddress(actor.getAddress());
					a.setEmail(actor.getEmail());
					a.setName(actor.getName());
					a.setMiddleName(actor.getMiddleName());
					a.setPhoneNumber(actor.getPhoneNumber());
					a.setSurname(actor.getSurname());
					a.setPhoto(actor.getPhoto());
					this.customerService.save(a);
				} else if (authorities.contains(Authority.HANDYWORKER)) {
					final HandyWorker a = this.handyWorkerService.findOne(actor.getId());
					a.setAddress(actor.getAddress());
					a.setEmail(actor.getEmail());
					a.setName(actor.getName());
					a.setMiddleName(actor.getMiddleName());
					a.setPhoneNumber(actor.getPhoneNumber());
					a.setSurname(actor.getSurname());
					a.setPhoto(actor.getPhoto());
					this.handyWorkerService.save(a);
				} else if (authorities.contains(Authority.REFEREE)) {
					final Referee a = this.refereeService.findOne(actor.getId());
					a.setAddress(actor.getAddress());
					a.setEmail(actor.getEmail());
					a.setName(actor.getName());
					a.setMiddleName(actor.getMiddleName());
					a.setPhoneNumber(actor.getPhoneNumber());
					a.setSurname(actor.getSurname());
					a.setPhoto(actor.getPhoto());
					this.refereeService.save(a);
				} else if (authorities.contains(Authority.SPONSOR)) {
					final Sponsor a = this.sponsorService.findOne(actor.getId());
					a.setAddress(actor.getAddress());
					a.setEmail(actor.getEmail());
					a.setName(actor.getName());
					a.setMiddleName(actor.getMiddleName());
					a.setPhoneNumber(actor.getPhoneNumber());
					a.setSurname(actor.getSurname());
					a.setPhoto(actor.getPhoto());
					this.sponsorService.save(a);
				}
				result = new ModelAndView("redirect:/welcome.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.personalDataModelAndView(actor, "profile.commit.error");
			}

		return result;
	}

	private ModelAndView personalDataModelAndView(final Actor actor) {

		return this.personalDataModelAndView(actor, null);
	}

	private ModelAndView personalDataModelAndView(final Actor actor, final String message) {

		final ModelAndView res = new ModelAndView("profile/actor/personalData");
		res.addObject("actor", actor);
		res.addObject("message", message);

		return res;

	}
}
