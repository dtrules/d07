
package controllers.actor;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.FolderService;
import services.MessageService;
import services.SponsorshipService;
import controllers.AbstractController;
import domain.Actor;
import domain.Folder;
import domain.Message;

@Controller
@RequestMapping("/message/actor")
public class MessageActorController extends AbstractController {

	//Services ----------------------------------------------------------
	@Autowired
	MessageService		messageService;

	@Autowired
	ActorService		actorService;

	@Autowired
	SponsorshipService	sponsorshipService;

	@Autowired
	FolderService		folderService;


	//List----------------------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam(required = false) final Integer folderId) {

		ModelAndView result;
		Collection<Message> messages;
		Collection<Folder> folders;

		final Actor actor = this.actorService.findByPrincipal();
		final Folder folder = this.folderService.findOne(folderId);
		final String folderName = this.folderService.findOne(folderId).getName();

		messages = this.messageService.getMessagesByFolder(folder);
		folders = actor.getFolders();
		folders.remove(folder);

		result = new ModelAndView("message/actor/list");
		result.addObject("folderName", folderName);
		result.addObject("folders", folders);
		result.addObject("messages", messages);

		return result;

	}

	//Create ---------------------------------------------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		ModelAndView res;
		System.out.println(this.actorService.findByPrincipal().getName());
		System.out.println(this.actorService.findByPrincipal().getId());

		final Message newMessage = this.messageService.create();

		res = this.createEditModelAndView(newMessage);
		res.addObject("newMessage", newMessage);

		return res;
	}
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Message newMessage, final BindingResult binding) {

		ModelAndView res;
		final Actor actor = this.actorService.findByPrincipal();
		if (binding.hasErrors()) {

			newMessage.setSender(actor);
			System.out.println("Moment: " + newMessage.getMoment());
			res = this.createEditModelAndView(newMessage);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.messageService.save(newMessage);
				res = new ModelAndView("redirect:/folder/actor/listSystem.do");
				//System.out.println("------------Errors:" + binding.getAllErrors());
			} catch (final Throwable error) {
				res = this.createEditModelAndView(newMessage, "folder.error.save");
			}
		return res;
	}
	//Broadcast ---------------------------------------------------------
	@RequestMapping(value = "/broadcast", method = RequestMethod.GET)
	public ModelAndView broadcast() {

		ModelAndView res;

		final Message newMessage = this.messageService.create();
		res = this.broadcastModelAndView(newMessage);

		return res;
	}

	@RequestMapping(value = "/broadcast", method = RequestMethod.POST, params = "save")
	public ModelAndView broadcast(final Message newMessage, final BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {

			res = this.broadcastModelAndView(newMessage);
			System.out.println(binding.getAllErrors());
		} else

			try {
				this.messageService.broadcast(newMessage);
				res = new ModelAndView("redirect:/folder/actor/listSystem.do");
			} catch (final Throwable error) {
				res = this.createEditModelAndView(newMessage, "folder.error.save");

			}

		return res;

	}

	//Display ------------------------------------------------------------
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView Display(@RequestParam final Integer messageId) {
		ModelAndView result;
		final Message newMessage = this.messageService.findOne(messageId);

		result = new ModelAndView("message/actor/display");
		result.addObject("newMessage", newMessage);

		return result;
	}

	//Move ---------------------------------------------------------
	@RequestMapping(value = "/move", method = RequestMethod.GET)
	public ModelAndView move(@RequestParam(required = true) final Integer messageId, @RequestParam(required = true) final Integer folderId) {
		ModelAndView res = null;
		final Message message = this.messageService.findOne(messageId);
		Assert.notNull(message);
		final Folder folder = this.folderService.findOne(folderId);
		Assert.notNull(folder);
		try {
			this.messageService.move(message, folder);
			res = new ModelAndView("redirect:list.do?folderId=" + folder.getId());
		} catch (final Throwable t) {
			res = new ModelAndView("redirect:list.do?folderId=" + folder.getId());
		}
		return res;
	}

	//Delete-----------------------------------------------------------------------

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int messageId) {

		ModelAndView res;

		final Message message = this.messageService.findOne(messageId);
		final Folder folder = this.folderService.findFolderByMessageId(messageId);

		try {
			this.messageService.delete(message);
		} catch (final Throwable error) {

		}
		res = new ModelAndView("redirect:/message/actor/list.do?folderId=" + folder.getId());

		return res;

	}

	//Ancillary methods-------------------------------------------------------------
	private ModelAndView createEditModelAndView(final Message newMessage) {
		return this.createEditModelAndView(newMessage, null);
	}

	private ModelAndView createEditModelAndView(final Message newMessage, final String text) {
		ModelAndView res;

		final Collection<Actor> recipients = this.actorService.findAll();
		final Actor actor = this.actorService.findByPrincipal();
		recipients.remove(actor);

		System.out.println("Recipients:" + recipients);
		res = new ModelAndView("message/actor/create");
		res.addObject("mess", newMessage);
		res.addObject("message", text);
		res.addObject("recipients", recipients);

		return res;
	}

	private ModelAndView broadcastModelAndView(final Message newMessage) {
		return this.broadcastModelAndView(newMessage, null);
	}

	private ModelAndView broadcastModelAndView(final Message newMessage, final String text) {
		ModelAndView res;

		res = new ModelAndView("message/actor/broadcast");
		res.addObject("mess", newMessage);
		res.addObject("message", text);

		return res;
	}
}
