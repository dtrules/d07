
package controllers.customer;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.CreditCardService;
import services.CustomerService;
import controllers.AbstractController;
import domain.CreditCard;

@Controller
@RequestMapping("/creditCard/customer")
public class CreditCardCustomerController extends AbstractController {

	//Constructors ----------------------

	public CreditCardCustomerController() {
		super();
	}


	//Services ---------------------
	@Autowired
	private CreditCardService	creditCardService;

	@Autowired
	private CustomerService		customerService;


	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final CreditCard creditCard = this.creditCardService.create();

		final ModelAndView result = this.createEditModelAndView(creditCard);

		return result;
	}
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final CreditCard creditCard, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(creditCard);
		else
			try {

				this.creditCardService.save(creditCard);

				result = new ModelAndView("redirect:/task/list.do");

			} catch (final Throwable oops) {
				result = this.createEditModelAndView(creditCard, "creditCard.commit.error");
			}
		return result;
	}

	private ModelAndView createEditModelAndView(final CreditCard creditCard) {

		return this.createEditModelAndView(creditCard, null);
	}

	private ModelAndView createEditModelAndView(final CreditCard creditCard, final String message) {

		final ModelAndView res = new ModelAndView("creditCard/customer/create");
		res.addObject("creditCard", creditCard);
		res.addObject("message", message);

		return res;

	}
}
