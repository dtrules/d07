package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.SponsorshipService;
import domain.Sponsorship;

@Controller
@RequestMapping("/sponsorship")
public class SponsorshipController extends AbstractController {
	
	//Constructors --------------------------------------
	
		public SponsorshipController(){
			super();
			
		}
		
		
		//Services ----------------------------------------------
		
		@Autowired
		private SponsorshipService sponsorshipService;

		
		
		@RequestMapping(value = "/display", method = RequestMethod.GET)
		public ModelAndView Display(@RequestParam final Integer sponsorshipId){
			ModelAndView result;
			Sponsorship sponsorship;
			
			sponsorship = this.sponsorshipService.findOne(sponsorshipId);
			
			result = new ModelAndView("sponsorship/display");
			result.addObject("sponsorship", sponsorship);
			
			return result;
		}
}
