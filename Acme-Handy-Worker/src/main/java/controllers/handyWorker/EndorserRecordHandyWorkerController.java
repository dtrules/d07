
package controllers.handyWorker;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.EndorserRecordService;
import services.HandyWorkerService;
import controllers.AbstractController;
import domain.EndorserRecord;

@Controller
@RequestMapping("/endorserRecord/handyWorker")
public class EndorserRecordHandyWorkerController extends AbstractController {

	@Autowired
	private EndorserRecordService	endorserRecordService;

	@Autowired
	private HandyWorkerService		handyWorkerService;


	// Constructors -----------------------------------------------------------

	public EndorserRecordHandyWorkerController() {
		super();
	}

	// Listing ----------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<EndorserRecord> endorserRecords;

		if (this.handyWorkerService.findByPrincipal() != null)
			endorserRecords = this.handyWorkerService.findByPrincipal().getCurriculum().getEndorserRecords();
		else
			endorserRecords = this.endorserRecordService.findAll();

		result = new ModelAndView("endorserRecord/list");
		result.addObject("requestURI", "endorserRecord/handyWorker/list.do");
		result.addObject("endorserRecords", endorserRecords);

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView result;
		EndorserRecord endorserRecord;

		endorserRecord = this.endorserRecordService.create();
		this.handyWorkerService.findByPrincipal().getCurriculum().getEndorserRecords().add(endorserRecord);
		result = this.createEditModelAndView(endorserRecord);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int endorserRecordId) {
		ModelAndView result;
		EndorserRecord endorserRecord;

		endorserRecord = this.endorserRecordService.findOne(endorserRecordId);
		result = this.createEditModelAndView(endorserRecord);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final EndorserRecord endorserRecord, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(endorserRecord);
		else
			try {
				this.endorserRecordService.save(endorserRecord);
				result = new ModelAndView("redirect:list.do");
			} catch (final Throwable oops) {
				result = this.createEditModelAndView(endorserRecord, "endorserRecord.commit.error");
			}

		return result;
	}

	protected ModelAndView createEditModelAndView(final EndorserRecord endorserRecord) {
		ModelAndView result;

		result = this.createEditModelAndView(endorserRecord, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(final EndorserRecord endorserRecord, final String message) {
		ModelAndView result;

		result = new ModelAndView("endorserRecord/edit");

		result.addObject("message", message);
		result.addObject("endorserRecord", endorserRecord);

		return result;

	}

	// Delete ---------------------------------------------------------------
	//
	//	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	//	public ModelAndView delete(@RequestParam final int educationRecordId) {
	//		ModelAndView result;
	//
	//		final EducationRecord educationRecord = this.educationRecordService.findOne(educationRecordId);
	//
	//		try {
	//
	//			this.educationRecordService.delete(educationRecord);
	//		} catch (final Throwable th) {
	//
	//		}
	//
	//		result = new ModelAndView("redirect:/educationRecord/handyWorker/list.do");
	//
	//		return result;
	//	}

}
