
package controllers.handyWorker;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.SectionService;
import services.SponsorshipService;
import services.TutorialService;
import controllers.AbstractController;
import domain.Section;
import domain.Sponsorship;
import domain.Tutorial;

@Controller
@RequestMapping("/tutorial/handyWorker")
public class TutorialHandyWorkerController extends AbstractController {

	//Constructor ----------------
	public TutorialHandyWorkerController() {
		super();
	}


	//Services --------------------------

	@Autowired
	private TutorialService		tutorialService;

	@Autowired
	private SectionService		sectionService;

	@Autowired
	private SponsorshipService	sponsorshipService;


	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final Tutorial tutorial = this.tutorialService.create();

		final ModelAndView result = this.createEditModelAndView(tutorial);

		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Tutorial tutorial, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(tutorial);
		else
			try {
				this.tutorialService.save(tutorial);
				System.out.println(binding);
				result = new ModelAndView("redirect;/tutorial/list.do");

			} catch (final Throwable oops) {
				result = this.createEditModelAndView(tutorial, "tutorial.comit.error");
			}
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int tutorialId) {
		ModelAndView result;
		Tutorial tutorial;
		Boolean isEmpty = false;
		final Collection<Sponsorship> sponsorships = this.sponsorshipService.findByTutorial(tutorialId);

		if (sponsorships.isEmpty())
			isEmpty = true;

		tutorial = this.tutorialService.findOne(tutorialId);
		result = this.createEditModelAndView(tutorial);
		result.addObject("tutorialId", tutorialId);
		result.addObject("isEmpty", isEmpty);

		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Tutorial tutorial, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(tutorial);
		//System.out.println(binding.getAllErrors());
		else
			try {
				this.tutorialService.save(tutorial);
				result = new ModelAndView("redirect:/tutorial/list.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.createEditModelAndView(tutorial, "tutorial.comit.error");
			}

		result.addObject("tutorialId", tutorial.getId());
		return result;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Tutorial tutorial, final BindingResult binding) {
		ModelAndView result;

		try {

			this.tutorialService.delete(tutorial);
			result = new ModelAndView("redirect:../list.do");
			result.addObject("tutorialId", tutorial.getId());

		}

		catch (final Throwable oops) {
			result = this.createEditModelAndView(tutorial, "tutorial.comit.error");
		}
		return result;
	}

	private ModelAndView createEditModelAndView(final Tutorial tutorial) {

		return this.createEditModelAndView(tutorial, null);
	}

	private ModelAndView createEditModelAndView(final Tutorial tutorial, final String message) {
		final Collection<Section> sections = this.sectionService.findSectionsWithoutTutorial();
		final ModelAndView res = new ModelAndView("tutorial/handyWorker/edit");
		res.addObject("sections", sections);
		res.addObject("tutorial", tutorial);
		res.addObject("message", message);

		return res;

	}
}
