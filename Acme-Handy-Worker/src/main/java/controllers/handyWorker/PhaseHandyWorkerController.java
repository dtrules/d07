/*
 * AdministratorController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.handyWorker;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.HandyWorkerService;
import services.PhaseService;
import controllers.AbstractController;
import domain.Application;
import domain.HandyWorker;
import domain.Phase;

@Controller
@RequestMapping("/phase/handyWorker")
public class PhaseHandyWorkerController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public PhaseHandyWorkerController() {
		super();
	}


	// Services ---------------------------------------------------------------
	@Autowired
	private PhaseService		phaseService;

	@Autowired
	private HandyWorkerService	handyWorkerService;


	// List ---------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(final String message) {

		final Collection<Phase> phases;

		phases = this.phaseService.findAll();

		final HandyWorker principal = this.handyWorkerService.findByPrincipal();

		final Collection<Phase> hwPhases = new ArrayList<Phase>();

		for (final Application a : principal.getApplications())
			hwPhases.addAll(a.getTask().getPhases());

		phases.retainAll(hwPhases);

		final ModelAndView result = new ModelAndView("phase/handyWorker/list");
		result.addObject("phases", phases);
		result.addObject("message", message);
		result.addObject("requestURI", "phase/handyWorker/list.do");

		return result;
	}

	// Create ---------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(required = true) final Integer taskId) {

		final Phase phase = this.phaseService.create();

		final ModelAndView result = this.createModelAndView(phase);
		result.addObject("taskId", taskId);

		return result;
	}

	// Save del create ---------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@RequestParam(required = true) final Integer taskId, @Valid final Phase phase, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = this.createModelAndView(phase);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.phaseService.saveCreate(phase, taskId);

				result = new ModelAndView("redirect:/phase/handyWorker/list.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.createModelAndView(phase, "phase.commit.error");
			}

		return result;
	}
	// Edit ---------------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int phaseId) {
		ModelAndView result;
		Phase phase;

		phase = this.phaseService.findOne(phaseId);
		result = this.createEditModelAndView(phase);

		return result;
	}

	// Save del Edit ---------------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Phase phase, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(phase);
		//System.out.println(binding.getAllErrors());
		else
			try {
				this.phaseService.save(phase);
				result = new ModelAndView("redirect:/phase/handyWorker/list.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.createEditModelAndView(phase, "phase.commit.error");
			}

		return result;
	}

	// Delete ---------------------------------------------------------------

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int phaseId) {
		ModelAndView result;

		final Phase phase = this.phaseService.findOne(phaseId);

		try {

			this.phaseService.delete(phase);
		} catch (final Throwable th) {

		}

		result = new ModelAndView("redirect:/phase/handyWorker/list.do");

		return result;
	}

	// Ancillary methods ---------------------------------------------------------------

	private ModelAndView createModelAndView(final Phase phase) {

		return this.createModelAndView(phase, null);
	}

	private ModelAndView createModelAndView(final Phase phase, final String message) {

		final ModelAndView res = new ModelAndView("phase/handyWorker/create");

		res.addObject("phase", phase);
		res.addObject("message", message);

		return res;

	}

	private ModelAndView createEditModelAndView(final Phase phase) {

		return this.createEditModelAndView(phase, null);
	}

	private ModelAndView createEditModelAndView(final Phase phase, final String message) {

		final ModelAndView res = new ModelAndView("phase/handyWorker/edit");

		res.addObject("phase", phase);
		res.addObject("message", message);

		return res;

	}

}
