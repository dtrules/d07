/*
 * CustomerController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.CustomerService;
import domain.Customer;

@Controller
@RequestMapping("/customer")
public class CustomerController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public CustomerController() {
		super();
	}


	// Services

	@Autowired
	CustomerService	customerService;


	// Display ---------------------------------------------------

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView Display(@RequestParam final Integer customerId) {
		ModelAndView result;
		Customer customer;

		customer = this.customerService.findOne(customerId);

		result = new ModelAndView("customer/display");
		result.addObject("customer", customer);

		return result;
	}

	// Create ----------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final Customer customer = this.customerService.create();

		final ModelAndView result = this.createEditModelAndView(customer);

		return result;
	}

	// Save del create -------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Customer customer, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(customer);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.customerService.save(customer);

				res = new ModelAndView("redirect:/");

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createEditModelAndView(customer, "customer.commit.error");
			}

		return res;
	}
	// Edit ------------------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		final Customer customer = this.customerService.findByPrincipal();

		Assert.notNull(customer);
		res = this.editModelAndView(customer);

		return res;
	}

	// Save del Edit----------------------------------------------

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Customer customer, final BindingResult binding) {
		ModelAndView res;
		System.out.println(binding.getAllErrors());
		if (binding.hasErrors())
			res = this.editModelAndView(customer);
		else
			try {
				this.customerService.save(customer);
				res = new ModelAndView("redirect:/");

			} catch (final Throwable oops) {
				res = this.editModelAndView(customer, "customer.commit.error");
			}

		return res;
	}

	// List ----------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView res;
		Collection<Customer> customers;

		customers = this.customerService.findAll();

		res = new ModelAndView("customer/list");
		res.addObject("requestURI", "customer/list.do");
		res.addObject("customers", customers);

		return res;
	}

	// Ancillary methods ---------------------------------------------------------------

	private ModelAndView createEditModelAndView(final Customer customer) {

		return this.createEditModelAndView(customer, null);
	}

	private ModelAndView createEditModelAndView(final Customer customer, final String message) {

		final ModelAndView res = new ModelAndView("customer/create");
		res.addObject("customer", customer);
		res.addObject("message", message);

		return res;

	}

	private ModelAndView editModelAndView(final Customer customer) {

		return this.editModelAndView(customer, null);
	}

	private ModelAndView editModelAndView(final Customer customer, final String message) {

		final ModelAndView res = new ModelAndView("customer/edit");
		res.addObject("customer", customer);
		res.addObject("message", message);

		return res;

	}
}
