/*
 * AdministratorController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.ArrayList;
import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.ApplicationService;
import services.CreditCardService;
import services.CustomerService;
import services.HandyWorkerService;
import services.TaskService;
import domain.Application;
import domain.CreditCard;
import domain.Customer;
import domain.Task;

@Controller
@RequestMapping("/application")
public class ApplicationController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public ApplicationController() {
		super();
	}


	// Services ---------------------------------------------------------------
	@Autowired
	private ApplicationService	applicationService;

	@Autowired
	private ActorService		actorService;

	@Autowired
	private CustomerService		customerService;

	@Autowired
	private CreditCardService	creditCardService;

	@Autowired
	private HandyWorkerService	handyWorkerService;

	@Autowired
	private TaskService			taskService;


	//Display ------------------------------------------------------------

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView Display(@RequestParam final Integer applicationId) {
		ModelAndView result;
		Application application;

		application = this.applicationService.findOne(applicationId);
		final double newApplicationPrice = this.applicationService.newApplicationPrice(application);

		result = new ModelAndView("application/display");
		result.addObject("application", application);
		result.addObject("applicationId", applicationId);
		result.addObject("newApplicationPrice", newApplicationPrice);

		return result;
	}

	// List ---------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(final String message) {

		Collection<Application> applications = new ArrayList<Application>();

		if (this.customerService.findByPrincipal() != null) {
			final Customer principal = this.customerService.findByPrincipal();
			applications = this.applicationService.getApplicationsByTasks(principal.getTasks());
		}

		if (this.handyWorkerService.findByPrincipal() != null)
			applications = this.handyWorkerService.findByPrincipal().getApplications();

		final ModelAndView result = new ModelAndView("application/list");
		result.addObject("applications", applications);
		result.addObject("message", message);
		result.addObject("requestURI", "application/list.do");

		return result;
	}
	// Create ---------------------------------------------------------------

	@RequestMapping(value = "/handyWorker/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final Application application = this.applicationService.create();

		final ModelAndView result = this.createEditModelAndView(application);
		return result;
	}

	// Edit ---------------------------------------------------------------

	@RequestMapping(value = "/handyWorker/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int applicationId) {
		ModelAndView result;
		Application application;

		application = this.applicationService.findOne(applicationId);
		result = this.createEditModelAndView(application);
		result.addObject("applicationId", applicationId);

		return result;
	}
	// Save del Edit ---------------------------------------------------------------

	@RequestMapping(value = "/handyWorker/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Application application, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(application);
		//System.out.println(binding.getAllErrors());
		else
			try {
				this.applicationService.save(application);
				result = new ModelAndView("redirect:/application/list.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.createEditModelAndView(application, "application.commit.error");
			}

		return result;
	}

	// Accept Application ---------------------------------------------------------------

	@RequestMapping(value = "/customer/creditCard", method = RequestMethod.GET)
	public ModelAndView accept(@RequestParam final int applicationId) {
		ModelAndView result;
		final Customer customer = this.customerService.findByPrincipal();

		final Collection<CreditCard> creditCards = customer.getCreditCards();
		final Application application = this.applicationService.findOne(applicationId);
		result = new ModelAndView("application/customer/creditCard");
		result.addObject("applicationId", applicationId);
		result.addObject("application", application);
		result.addObject("creditCards", creditCards);

		return result;
	}
	@RequestMapping(value = "/customer/creditCard", method = RequestMethod.POST, params = "save")
	public ModelAndView saveAccept(@Valid final Application application, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.editModelAndView(application);
		//System.out.println(binding.getAllErrors());
		else
			try {
				this.applicationService.save(application);
				result = new ModelAndView("redirect:/application/list.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.editModelAndView(application, "application.commit.error");
			}

		return result;
	}

	// Ancillary methods ---------------------------------------------------------------

	private ModelAndView createEditModelAndView(final Application application) {

		return this.createEditModelAndView(application, null);
	}

	private ModelAndView createEditModelAndView(final Application application, final String message) {
		final Collection<Task> tasks = this.taskService.findTasksToApplyForHandyWorker();
		final ModelAndView res = new ModelAndView("application/handyWorker/edit");
		res.addObject("application", application);
		res.addObject("message", message);
		res.addObject("tasks", tasks);

		return res;

	}

	private ModelAndView editModelAndView(final Application application) {

		return this.editModelAndView(application, null);
	}

	private ModelAndView editModelAndView(final Application application, final String message) {
		final ModelAndView res = new ModelAndView("application/customer/creditCard");
		res.addObject("application", application);
		res.addObject("message", message);

		return res;

	}

}
