/*
 * TaskController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.CategoryService;
import services.CustomerService;
import services.TaskService;
import services.WarrantyService;
import domain.Category;
import domain.Task;
import domain.Warranty;

@Controller
@RequestMapping("/task")
public class TaskController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public TaskController() {
		super();
	}


	// Services ---------------------------------------------------------------
	@Autowired
	private TaskService		taskService;

	@Autowired
	private CustomerService	customerService;

	@Autowired
	private WarrantyService	warrantyService;

	@Autowired
	private CategoryService	categoryService;


	// List ---------------------------------------------------------------

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(final String message, @RequestParam(required = false) final String searchContent, @RequestParam(required = false) final String searchCategory, @RequestParam(required = false) final String searchWarranty) {

		final ModelAndView result = new ModelAndView("task/list");
		Collection<Task> tasks = null;
		String requestURI = "task/list.do";

		if (searchContent == null && searchCategory == null && searchWarranty == null) {
			if (this.customerService.findByPrincipal() != null)
				tasks = this.customerService.findByPrincipal().getTasks();
			else
				tasks = this.taskService.findAll();
		} else if (searchContent != null) {
			if (this.customerService.findByPrincipal() != null) {
				final Collection<Task> customerTasks = this.customerService.findByPrincipal().getTasks();
				final Collection<Task> allTasks = this.taskService.searchTasks(searchContent);
				allTasks.retainAll(customerTasks);
				tasks = allTasks;
			} else {
				tasks = this.taskService.searchTasks(searchContent);
				requestURI += "?searchContent" + searchContent;
			}
		} else if (searchCategory != null) {
			if (this.customerService.findByPrincipal() != null) {
				final Collection<Task> customerTasks = this.customerService.findByPrincipal().getTasks();
				final Collection<Task> allTasks = this.taskService.searchTasksByCategory(searchCategory);
				allTasks.retainAll(customerTasks);
				tasks = allTasks;
			} else {
				tasks = this.taskService.searchTasksByCategory(searchCategory);
				requestURI += "?searchCategory" + searchCategory;
			}
		} else if (searchWarranty != null)
			if (this.customerService.findByPrincipal() != null) {
				final Collection<Task> customerTasks = this.customerService.findByPrincipal().getTasks();
				final Collection<Task> allTasks = this.taskService.searchTasksByWarranty(searchWarranty);
				allTasks.retainAll(customerTasks);
				tasks = allTasks;
			} else {
				tasks = this.taskService.searchTasksByWarranty(searchWarranty);
				requestURI += "?searchWarranty" + searchWarranty;
			}

		result.addObject("tasks", tasks);
		result.addObject("message", message);
		result.addObject("requestURI", requestURI);

		return result;
	}
	// Display ---------------------------------------------------------------

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView Display(@RequestParam final Integer taskId) {
		ModelAndView result;
		Task task;

		task = this.taskService.findOne(taskId);
		final double newTaskPrice = this.taskService.newTaskPrice(task);

		final boolean canCreatePhase = this.taskService.canCreatePhase(taskId);

		result = new ModelAndView("task/display");
		result.addObject("task", task);
		result.addObject("taskId", taskId);
		result.addObject("canCreatePhase", canCreatePhase);
		result.addObject("newTaskPrice", newTaskPrice);

		return result;
	}

	// Create ---------------------------------------------------------------

	@RequestMapping(value = "/customer/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final Task task = this.taskService.create();

		final ModelAndView result = this.createEditModelAndView(task);

		return result;
	}

	// Save del create ---------------------------------------------------------------

	@RequestMapping(value = "/customer/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Task task, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(task);
		//System.out.println(binding.getAllErrors());
		else
			try {
				this.taskService.save(task);
				result = new ModelAndView("redirect:/task/list.do");

			} catch (final Throwable oops) {
				//				System.out.println(oops);
				result = this.createEditModelAndView(task, "task.commit.error");
			}

		return result;
	}

	// Edit ---------------------------------------------------------------

	@RequestMapping(value = "/customer/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int taskId) {
		ModelAndView result;
		Task task;

		task = this.taskService.findOne(taskId);
		result = this.createEditModelAndView(task);

		return result;
	}

	// Save del Edit ---------------------------------------------------------------

	@RequestMapping(value = "/customer/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Task task, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(task);
		//System.out.println(binding.getAllErrors());
		else
			try {
				this.taskService.save(task);
				result = new ModelAndView("redirect:/task/list.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.createEditModelAndView(task, "task.commit.error");
			}

		return result;
	}
	// Delete ---------------------------------------------------------------

	@RequestMapping(value = "/customer/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam final int taskId) {
		ModelAndView result;

		final Task task = this.taskService.findOne(taskId);
		this.taskService.delete(task);

		result = new ModelAndView("redirect:/task/list.do");

		return result;
	}

	// Ancillary methods ---------------------------------------------------------------

	private ModelAndView createEditModelAndView(final Task task) {

		return this.createEditModelAndView(task, null);
	}

	private ModelAndView createEditModelAndView(final Task task, final String message) {

		final Collection<Warranty> warranties = this.warrantyService.findWarrantiesNoDraft();
		final Collection<Category> categories = this.categoryService.findAll();

		final ModelAndView res = new ModelAndView("task/customer/edit");
		res.addObject("task", task);
		res.addObject("message", message);
		res.addObject("warranties", warranties);
		res.addObject("categories", categories);

		return res;

	}

}
