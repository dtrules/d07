
package controllers.sponsor;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.SponsorshipService;
import services.TutorialService;
import controllers.AbstractController;
import domain.Sponsorship;
import domain.Tutorial;

@Controller
@RequestMapping("/sponsorship/sponsor")
public class SponsorshipSponsorController extends AbstractController {

	//Constructor --------------------------

	public SponsorshipSponsorController() {
		super();
	}


	//Services ----------------------

	@Autowired
	private SponsorshipService	sponsorshipService;

	@Autowired
	private TutorialService		tutorialService;


	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {

		final Sponsorship sponsorship = this.sponsorshipService.create();

		final ModelAndView result = this.createEditModelAndView(sponsorship);
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView saveCreate(@Valid final Sponsorship sponsorship, final BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors())
			result = this.createEditModelAndView(sponsorship);
		//System.out.println(binding.getAllErrors());
		else
			try {

				System.out.println(binding.getAllErrors());
				this.sponsorshipService.save(sponsorship);

				result = new ModelAndView("redirect:/tutorial/list.do");

			} catch (final Throwable oops) {
				//System.out.println(oops);
				result = this.createEditModelAndView(sponsorship, "sponsorship.commit.error");
			}

		return result;
	}
	private ModelAndView createEditModelAndView(final Sponsorship sponsorship) {

		return this.createEditModelAndView(sponsorship, null);
	}

	private ModelAndView createEditModelAndView(final Sponsorship sponsorship, final String message) {
		final Collection<Tutorial> tutorials = this.tutorialService.findTutorialWithSections();
		final ModelAndView res = new ModelAndView("sponsorship/sponsor/create");
		res.addObject("sponsorship", sponsorship);
		res.addObject("message", message);
		res.addObject("tutorials", tutorials);

		return res;

	}
}
