
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@Access(AccessType.PROPERTY)
public class Customer extends Actor {

	// Attributes

	private Collection<CustomerEndorsement>	customerEndorsements;
	private Collection<Task>				tasks;
	private Collection<CreditCard>			creditCards;


	// Getters & setters

	// Relationships ----------------------------------------------------------

	@OneToMany(cascade = {
		CascadeType.ALL
	})
	public Collection<CreditCard> getCreditCards() {
		return this.creditCards;
	}

	public void setCreditCards(final Collection<CreditCard> creditCards) {
		this.creditCards = creditCards;
	}

	@OneToMany
	public Collection<CustomerEndorsement> getCustomerEndorsements() {
		return this.customerEndorsements;
	}

	public void setCustomerEndorsements(final Collection<CustomerEndorsement> customerEndorsements) {
		this.customerEndorsements = customerEndorsements;
	}

	@OneToMany
	public Collection<Task> getTasks() {
		return this.tasks;
	}

	public void setTasks(final Collection<Task> tasks) {
		this.tasks = tasks;
	}
}
