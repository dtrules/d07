
package converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import repositories.ActorRepository;
import repositories.AdministratorRepository;
import repositories.CustomerRepository;
import repositories.HandyWorkerRepository;
import repositories.RefereeRepository;
import repositories.SponsorRepository;
import domain.Actor;

@Component
@Transactional
public class StringToActorConverter implements Converter<String, Actor> {

	@Autowired
	ActorRepository			actorRepository;

	@Autowired
	AdministratorRepository	administratorRepository;

	@Autowired
	HandyWorkerRepository	handyWorkerRepository;

	@Autowired
	CustomerRepository		customerRepository;

	@Autowired
	RefereeRepository		refereeRepository;

	@Autowired
	SponsorRepository		sponsorRepository;


	@Override
	public Actor convert(final String text) {
		Actor result;
		int id;

		try {
			if (StringUtils.isEmpty(text))
				result = null;
			else {
				id = Integer.valueOf(text);
				result = this.administratorRepository.findOne(id);
				if (result == null)
					result = this.customerRepository.findOne(id);
				if (result == null)
					result = this.refereeRepository.findOne(id);
				if (result == null)
					result = this.sponsorRepository.findOne(id);
				if (result == null)
					result = this.handyWorkerRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}

		return result;
	}

}
