package converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import repositories.CustomerEndorsementRepository;

import domain.CustomerEndorsement;

@Component
@Transactional
public class StringToCustomerEndorsementConverter implements Converter<String, CustomerEndorsement>{

	@Autowired
	CustomerEndorsementRepository	customerEndorsementRepository;

	public CustomerEndorsement convert(final String text) {
		CustomerEndorsement result;
		int id;

		try {
			if (StringUtils.isEmpty(text))
				result = null;
			else {
				id = Integer.valueOf(text);
				result = this.customerEndorsementRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}

		return result;
	}
}
