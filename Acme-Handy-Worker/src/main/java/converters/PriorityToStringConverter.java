
package converters;

import java.net.URLEncoder;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Priority;

@Component
@Transactional
public class PriorityToStringConverter implements Converter<Priority, String> {

	@Override
	public String convert(final Priority priority) {
		String result;
		StringBuilder builder;

		if (priority == null)
			result = null;
		else
			try {
				builder = new StringBuilder();
				builder.append(URLEncoder.encode(priority.getValue(), "UTF-8"));
				result = builder.toString();
			} catch (final Throwable oops) {
				throw new RuntimeException(oops);
			}

		return result;
	}

}
