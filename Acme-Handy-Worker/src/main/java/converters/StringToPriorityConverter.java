
package converters;

import java.net.URLDecoder;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Priority;

@Component
@Transactional
public class StringToPriorityConverter implements Converter<String, Priority> {

	@Override
	public Priority convert(final String text) {
		Priority result;
		String parts[];

		if (text == null)
			result = null;
		else
			try {
				parts = text.split("\\|");
				result = new Priority();
				result.setValue(URLDecoder.decode(parts[0], "UTF-8"));

			} catch (final Throwable oops) {
				throw new RuntimeException(oops);
			}

		return result;
	}

}
