
package converters;

import java.net.URLDecoder;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Status;

@Component
@Transactional
public class StringToStatusConverter implements Converter<String, Status> {

	@Override
	public Status convert(final String text) {
		Status result;
		String parts[];

		if (text == null)
			result = null;
		else
			try {
				parts = text.split("\\|");
				result = new Status();
				result.setValue(URLDecoder.decode(parts[0], "UTF-8"));

			} catch (final Throwable oops) {
				throw new RuntimeException(oops);
			}

		return result;
	}

}
