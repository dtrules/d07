/*
 * LoginController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package security;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import controllers.AbstractController;
import domain.Actor;

@Controller
@RequestMapping("/security")
public class LoginController extends AbstractController {

	// Supporting services ----------------------------------------------------

	@Autowired
	LoginService	service;

	@Autowired
	ActorService	actorService;


	// Constructors -----------------------------------------------------------

	public LoginController() {
		super();
	}

	// Login ------------------------------------------------------------------

	@RequestMapping("/login")
	public ModelAndView login(@Valid final Credentials credentials, final BindingResult bindingResult, @RequestParam(required = false) final boolean showError, @RequestParam(required = false) final boolean isBanned) {

		Assert.notNull(credentials);
		Assert.notNull(bindingResult);

		final String loginUsername = credentials.getUsername();
		final Actor loginActor = this.actorService.findByUserAccountUsername(loginUsername);

		ModelAndView result = null;

		if (loginActor == null) {
			result = new ModelAndView("security/login");
			result.addObject("credentials", credentials);
			result.addObject("showError", showError);
		} else if (loginActor.getIsBanned()) {
			//result = new ModelAndView("redirect:/banFailure");
			result = new ModelAndView("security/login");
			result.addObject("credentials", credentials);
			result.addObject("isBanned", isBanned);
		}

		return result;
	}

	// LoginFailure -----------------------------------------------------------

	@RequestMapping("/loginFailure")
	public ModelAndView failure() {
		ModelAndView result;

		result = new ModelAndView("redirect:login.do?showError=true");

		return result;
	}

	// BannedFailure ------------------------------------------------------------

	@RequestMapping("/banFailure")
	public ModelAndView banned() {
		ModelAndView result;

		result = new ModelAndView("redirect:login.do?isBanned=true");

		return result;
	}

}
