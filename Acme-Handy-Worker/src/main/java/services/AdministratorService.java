
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.AdministratorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountRepository;
import domain.Actor;
import domain.Administrator;
import domain.Profile;

@Service
@Transactional
public class AdministratorService {

	// Managed Repository ---------------------------------------------------------
	@Autowired
	private AdministratorRepository	administratorRepository;

	// Supporting services ---------------------------------------------------------
	@Autowired
	private ActorService			actorService;

	@Autowired
	private FolderService			folderService;

	@Autowired
	private UserAccountRepository	userAccountRepository;


	// Constructor methods ---------------------------------------------------------
	public AdministratorService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------------

	public Administrator create() {

		final UserAccount userAccount = new UserAccount();
		final Administrator res = new Administrator();

		final Authority authority = new Authority();
		authority.setAuthority(Authority.ADMIN);

		Collection<Authority> authorities;

		authorities = userAccount.getAuthorities();
		authorities.add(authority);
		userAccount.setAuthorities(authorities);

		final Collection<Profile> profiles = new ArrayList<Profile>();
		res.setProfiles(profiles);
		res.setIsBanned(false);
		res.setIsSuspicious(false);
		res.setUserAccount(userAccount);
		res.setFolders(this.folderService.createSystemFolders());

		return res;
	}

	public Administrator save(final Administrator administrator) {
		Assert.notNull(administrator);
		if (administrator.getId() == 0) {
			final Md5PasswordEncoder encoder = new Md5PasswordEncoder();
			final UserAccount userAccount = administrator.getUserAccount();
			final String password = userAccount.getPassword();
			final String hashedPassword = encoder.encodePassword(password, null);
			userAccount.setPassword(hashedPassword);
			final UserAccount ua = this.userAccountRepository.save(userAccount);
			administrator.setUserAccount(ua);

		}
		this.actorService.checkAuth(Authority.ADMIN);
		final Administrator res = this.administratorRepository.save(administrator);
		return res;
	}

	public Administrator findOne(final int administratorId) {
		Assert.isTrue(administratorId != 0);
		Administrator result;

		result = this.administratorRepository.findOne(administratorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Administrator> findAll() {
		Collection<Administrator> result;

		result = this.administratorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	// Check if the actual user is an admin
	public void checkIfAdmin() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.ADMIN))
				res = true;
		Assert.isTrue(res);
	}

	// Finds the actual admin
	public Administrator findByPrincipal() {
		Administrator result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.administratorRepository.findAdministratorByUserAccount(userAccount.getId());
		Assert.notNull(result);

		return result;
	}

	//	// Find an admin by username
	//	public Administrator findAdministratorByUsername(final String username) {
	//		return this.administratorRepository.findAdministratorByUsername(username);
	//	}

	// Find administrator by useraccount
	public Administrator findAdministratorByUserAccount(final UserAccount userAccount) {
		return this.administratorRepository.findAdministratorByUserAccount(userAccount.getId());
	}

	public void checkAdministrator(final Administrator administrator) {
		Boolean result = true;

		if (administrator.getAddress() == null || administrator.getFolders() == null || administrator.getName() == null || administrator.getSurname() == null)
			result = false;

		Assert.isTrue(result);
	}

	// Get all spammers
	public Collection<Actor> findSuspicious() {
		this.actorService.checkAuth(Authority.ADMIN);
		return this.administratorRepository.findSuspicious();
	}

	// Ban action
	public void ban(final Actor a) {
		this.actorService.checkAuth(Authority.ADMIN);
		Assert.notNull(a);
		Assert.isTrue(a.getIsSuspicious());
		Assert.isTrue(a.getIsBanned() == false);
		a.setIsBanned(true);
		this.actorService.save(a);
	}

	// Unban action
	public void unban(final Actor a) {
		this.actorService.checkAuth(Authority.ADMIN);
		Assert.notNull(a);
		//Assert.isTrue(this.findSuspicious().contains(a)); //No hace falta ya que queremos desbanear simplemente
		Assert.isTrue(a.getIsBanned());
		a.setIsBanned(false);
		this.actorService.save(a);
	}
}
