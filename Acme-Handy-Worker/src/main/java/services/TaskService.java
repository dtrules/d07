
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.TaskRepository;
import security.LoginService;
import security.UserAccount;
import domain.Application;
import domain.Category;
import domain.Complaint;
import domain.Customer;
import domain.Finder;
import domain.HandyWorker;
import domain.Phase;
import domain.Task;
import domain.Warranty;

@Service
@Transactional
public class TaskService {

	//Managed repository ------------------------------------------------

	@Autowired
	private TaskRepository			taskRepository;

	//Other Services ---------------------------------------------
	@Autowired
	private CustomerService			customerService;

	@Autowired
	private CategoryService			categoryService;

	@Autowired
	private ActorService			actorService;

	@Autowired
	private HandyWorkerService		handyWorkerService;

	@Autowired
	private CustomizationService	customizationService;


	// Constructor methods ---------------------------------------------------------
	public TaskService() {
		super();
	}

	//Simple CRUD methods ------------------------

	public Task create() {

		UserAccount userAccount;
		final Warranty warranty = new Warranty();
		final Category category = new Category();
		final Collection<Phase> phases = new ArrayList<Phase>();
		final Collection<Complaint> complaints = new ArrayList<Complaint>();
		final Collection<Application> applications = new ArrayList<Application>();
		String ticker = "";
		final Date moment = new Date();
		String year = new Integer(moment.getYear() % 100).toString();
		String month = new Integer(moment.getMonth() + 1).toString();
		String day = new Integer(moment.getDate()).toString();
		if (year.length() == 1)
			year = "0" + year;
		if (month.length() == 1)
			month = "0" + month;
		if (day.length() == 1)
			day = "0" + day;

		final String l = RandomStringUtils.randomAlphanumeric(6);

		ticker = ticker + year + month + day + "-" + l.toUpperCase();

		userAccount = LoginService.getPrincipal();
		this.customerService.checkIfCustomer();

		final Task res = new Task();

		res.setTicker(ticker);
		res.setComplaints(complaints);
		res.setPhases(phases);
		res.setWarranty(warranty);
		res.setCategory(category);
		res.setApplications(applications);
		res.setPublishDate(moment);

		return res;
	}

	public Task save(final Task task) {
		this.customerService.checkIfCustomer();
		Task res = new Task();
		Assert.notNull(task);
		if (task.getCategory() == null)
			task.setCategory(this.categoryService.findCATEGORY()); //Si no se asigna categoria a task, por defecto se establece la Categoria Padre.
		this.checkTask(task);
		this.actorService.checkSpamWords(task.getAddress());
		this.actorService.checkSpamWords(task.getDescription());
		res = this.taskRepository.save(task);
		final Customer customer = this.customerService.findByPrincipal();
		customer.getTasks().add(res);
		return res;

	}

	public void delete(final Task task) {
		//		final Collection<Finder> finders = this.finderByTask(task.getId());
		//		final Customer customer = this.customerByTask(task.getId());
		Assert.isTrue(task.getApplications().isEmpty());
		this.customerService.checkIfCustomer();
		final Customer customer = this.customerService.findByPrincipal();
		Assert.notNull(task.getId());
		Assert.isTrue(customer.getTasks().contains(task));
		customer.getTasks().remove(task);
		this.taskRepository.delete(task.getId());
	}

	public void checkTask(final Task task) {
		Boolean result = true;

		if (task.getAddress() == null || task.getDescription() == null || task.getEndMoment() == null || task.getPublishDate() == null || task.getStartMoment() == null || task.getTicker() == null || task.getEndMoment().before(task.getStartMoment()))
			result = false;

		Assert.isTrue(result);
	}

	public Collection<Task> searchTasks(final String keyword) {
		return this.taskRepository.searchTasks(keyword);
	}

	public Collection<Task> searchTasksByCategory(final String keyword) {
		return this.taskRepository.searchTasksByCategory(keyword);
	}

	public Collection<Task> searchTasksByWarranty(final String keyword) {
		return this.taskRepository.searchTasksByWarranty(keyword);
	}

	public Task findOne(final int taskId) {
		Assert.isTrue(taskId != 0);
		Task result;

		result = this.taskRepository.findOne(taskId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Task> findAll() {

		Collection<Task> result;

		result = this.taskRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Collection<Finder> finderByTask(final int taskId) {
		return this.finderByTask(taskId);
	}

	public Customer customerByTask(final int taskId) {
		return this.customerByTask(taskId);
	}

	public Collection<Application> applicationByTask(final int taskId) {
		return this.taskRepository.applicationByTask(taskId);
	}

	public boolean canCreatePhase(final int taskId) {
		boolean result = false;

		final Task task = this.findOne(taskId);
		Assert.notNull(task);

		final HandyWorker principal = this.handyWorkerService.findByPrincipal();
		if (principal != null) {
			final Collection<Application> apps = task.getApplications();
			apps.retainAll(principal.getApplications());
			if (apps.isEmpty() == false)
				for (final Application a : apps)
					if (a.getStatus().getValue().equals("ACCEPTED")) {
						result = true;
						break;
					}
		}

		return result;
	}

	public Collection<Task> findTasksToApplyForHandyWorker() {
		final HandyWorker handyWorker = this.handyWorkerService.findByPrincipal();
		final Collection<Task> tasks = this.taskRepository.findAll();
		final Collection<Task> tasksApplayable = new ArrayList<Task>();
		for (final Task t : tasks)
			if (!this.canCreatePhase(t.getId()))
				tasksApplayable.add(t);

		return tasksApplayable;
	}
	//----------------------------Dashboard--------------------------------

	//Query 1

	public Double minNumberOfComplaints() {
		Double res;
		res = this.taskRepository.minComplaintsPerTask();
		return res;
	}

	public Double maxNumberOfComplaints() {
		Double res;
		res = this.taskRepository.maxComplaintsPerTask();
		return res;
	}

	public Double avgNumberOfComplaints() {
		Double res;
		res = this.taskRepository.avgComplaintsPerTask();
		return res;
	}

	public Double deviationNumberOfComplaints() {
		Double res;
		res = this.taskRepository.deviationComplaintsPerTask();
		return res;
	}

	//Query 4
	public Double minNumberOfApplications() {
		Double res;
		res = this.taskRepository.minApplicationsPerTask();
		return res;
	}

	public Double maxNumberOfApplications() {
		Double res;
		res = this.taskRepository.maxApplicationsPerTask();
		return res;
	}

	public Double avgNumberOfApplications() {
		Double res;
		res = this.taskRepository.avgApplicationsPerTask();
		return res;
	}

	public Double deviationNumberOfApplications() {
		Double res;
		res = this.taskRepository.deviationApplicationsPerTask();
		return res;
	}

	//Query 5
	public Double minMaxPricePerTask() {
		Double res;
		res = this.taskRepository.minMaxPricePerTask();
		return res;
	}

	public Double maxMaxPricePerTask() {
		Double res;
		res = this.taskRepository.maxMaxPricePerTask();
		return res;
	}

	public Double avgMaxPricePerTask() {
		Double res;
		res = this.taskRepository.avgMaxPricePerTask();
		return res;
	}

	public Double deviationMaxPricePerTask() {
		Double res;
		res = this.taskRepository.deviationMaxPricePerTask();
		return res;
	}

	public Double ratioOfTasksWithAComplaint() {
		Double res;
		res = this.taskRepository.ratioTasksWithAComplaint();
		return res;
	}

	public void saveComplaint(final Task task) {
		this.taskRepository.save(task);
	}

	public double newTaskPrice(final Task task) {
		double newPrice = 0.0;
		final Double vat = this.customizationService.findCustomization().getVat();
		newPrice = ((task.getMaxPrice() * vat) + task.getMaxPrice());
		return newPrice;
	}

}
