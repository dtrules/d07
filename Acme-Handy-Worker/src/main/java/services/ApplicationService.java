
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ApplicationRepository;
import security.Authority;
import domain.Application;
import domain.Customer;
import domain.HandyWorker;
import domain.Message;
import domain.Priority;
import domain.Status;
import domain.Task;

@Service
@Transactional
public class ApplicationService {

	//Managed repository ----------------------------

	@Autowired
	private ApplicationRepository	applicationRepository;

	//Supported Services ----------------------------

	@Autowired
	private ActorService			actorService;

	@Autowired
	private TaskService				taskService;

	@Autowired
	private HandyWorkerService		handyWorkerService;

	@Autowired
	private CustomerService			customerService;

	@Autowired
	private CustomizationService	customizationService;

	@Autowired
	private MessageService			messageService;


	// Contructor methods
	public ApplicationService() {
		super();
	}

	// Simple CRUD methods

	public Application create() {
		this.actorService.checkAuth(Authority.HANDYWORKER);
		final Integer cvvCode = 0;
		final Application res = new Application();

		final Collection<String> comments = Collections.<String> emptySet();
		res.setComments(comments);

		final Date moment = new Date();
		res.setMoment(moment);

		final Status status = new Status();
		status.setValue("PENDING");
		res.setStatus(status);

		final Task task = new Task();
		res.setTask(task);

		//		final CreditCard creditCard = new CreditCard();
		//		creditCard.setCvvCode(cvvCode);
		//		res.setCreditCard(creditCard);

		Assert.notNull(res);

		return res;
	}

	public Collection<Application> findAll() {
		final Collection<Application> res = this.applicationRepository.findAll();

		Assert.notNull(res);

		return res;
	}

	public Application findOne(final int applicationId) {
		Assert.isTrue(applicationId > 0);

		final Application res = this.applicationRepository.findOne(applicationId);

		Assert.notNull(res);

		return res;
	}

	public Application save(final Application application) {
		Assert.notNull(application);

		Application res;
		if (application.getId() != 0) {
			this.checkApplication(application);
			this.actorService.checkAuth(Authority.HANDYWORKER, Authority.CUSTOMER);
			if (this.customerService.findByPrincipal() != null)
				if (application.getStatus().getValue().equals("ACCEPTED"))
					Assert.notNull(application.getCreditCard());

			res = this.applicationRepository.save(application);

			this.communicateChange(application, this.customerService.findByPrincipal());
		} else {

			this.actorService.checkAuth(Authority.HANDYWORKER);
			final HandyWorker handyWorker = this.handyWorkerService.findByPrincipal();

			final Task task = application.getTask();
			res = this.applicationRepository.save(application);
			task.getApplications().add(res);
			handyWorker.getApplications().add(res);
		}
		return res;
	}
	public void delete(final Application application) {
		Assert.notNull(application);
		this.actorService.checkAuth(Authority.HANDYWORKER);
		Assert.isTrue(application.getId() != 0);
		Assert.isTrue(this.applicationRepository.exists(application.getId()));

		this.applicationRepository.delete(application);
	}

	public void acceptApplication(final int applicationId) {
		Assert.isTrue(applicationId != 0);
		final Application application = this.findOne(applicationId);
		Assert.notNull(application);

		this.actorService.checkAuth(Authority.CUSTOMER);

		final Status status = new Status();
		status.setValue("ACCEPTED");
		application.setStatus(status);
		this.applicationRepository.save(application);
	}

	public void rejectApplication(final int applicationId) {
		Assert.isTrue(applicationId != 0);
		final Application application = this.findOne(applicationId);
		Assert.notNull(application);

		this.actorService.checkAuth(Authority.CUSTOMER);

		final Status status = new Status();
		status.setValue("REJECTED");
		application.setStatus(status);
		this.applicationRepository.save(application);

	}

	public void communicateChange(final Application application, final Customer c) {
		final HandyWorker hw = this.handyWorkerService.findHandyWorkerByApplicationId(application.getId());
		final Message cMessage = this.messageService.create();
		final Message hwMessage = this.messageService.create();

		final String subject = "Change on an Application/Cambio en una aplicación";
		final String body1 = "Application of Task: " + application.getTask().getDescription() + " has changed his status.\n";
		final String body2 = "La aplicación de la Tarea: " + application.getTask().getDescription() + " ha cambiado su estado.\n";
		final String body = body1 + body2;
		final Priority priority = new Priority();
		priority.setValue("HIGH");
		final Collection<String> tags = new ArrayList<String>();
		tags.add("Application");
		tags.add("Status");
		tags.add("Change");

		cMessage.setSubject(subject);
		cMessage.setBody(body);
		cMessage.setPriority(priority);
		cMessage.setTags(tags);
		cMessage.setRecipient(c);

		hwMessage.setSubject(subject);
		hwMessage.setBody(body);
		hwMessage.setPriority(priority);
		hwMessage.setTags(tags);
		hwMessage.setRecipient(hw);

		this.messageService.save(cMessage);
		this.messageService.save(hwMessage);
	}
	//Check Application
	public void checkApplication(final Application application) {
		Boolean res = true;

		if (application.getStatus() == null || application.getTask() == null || application.getComments() == null || application.getMoment() == null)
			res = false;

		Assert.isTrue(res);
	}

	public Collection<Application> getApplicationsByTask(final int taskId) {
		return this.applicationRepository.getApplicationsByTask(taskId);
	}

	public Collection<Application> getApplicationsByTasks(final Collection<Task> tasks) {
		final Collection<Application> result = new ArrayList<Application>();

		for (final Task t : tasks)
			result.addAll(this.getApplicationsByTask(t.getId()));
		return result;
	}

	public Double minPricePerApplication() {
		Double res;
		res = this.applicationRepository.minPricePerApplication();
		return res;
	}

	public Double maxPricePerApplication() {
		Double res;
		res = this.applicationRepository.maxPricePerApplication();
		return res;
	}

	public Double avgPricePerApplication() {
		Double res;
		res = this.applicationRepository.avgPricePerApplication();
		return res;
	}

	public Double deviationPricePerApplication() {
		Double res;
		res = this.applicationRepository.deviationPricePerApplication();
		return res;
	}

	public Double ratioOfPendingApplications() {
		Double res;
		res = this.applicationRepository.findRatioOfPendingApplications();
		return res;
	}

	public Double ratioOfAcceptedApplications() {
		Double res;
		res = this.applicationRepository.findRatioOfAcceptedApplications();
		return res;
	}

	public Double ratioOfRejectedApplications() {
		Double res;
		res = this.applicationRepository.findRatioOfRejectedApplications();
		return res;
	}

	public Double ratioOfPendingApplicationsElapsed() {
		Double res;
		res = this.applicationRepository.findRatioOfPendingApplicationsElapsed();
		return res;
	}
	public double newApplicationPrice(final Application application) {
		double newPrice = 0.0;
		final Double vat = this.customizationService.findCustomization().getVat();
		newPrice = ((application.getPrice() * vat) + application.getPrice());
		return newPrice;
	}
}
