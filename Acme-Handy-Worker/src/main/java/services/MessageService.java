
package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.MessageRepository;
import security.Authority;
import domain.Actor;
import domain.Folder;
import domain.Message;

@Service
@Transactional
public class MessageService {

	//Managed repositories-------------------------------------------
	@Autowired
	private MessageRepository		messageRepository;

	//Supported services---------------------------------------------
	@Autowired
	private FolderService			folderService;

	@Autowired
	private ActorService			actorService;

	@Autowired
	private CustomizationService	customizationService;


	//Constructor----------------------------------------------------
	public MessageService() {
		super();
	}

	//Simple CRUD methods--------------------------------------------
	public Message create() {
		final Message res = new Message();
		final Actor sender = this.actorService.findByPrincipal();

		Assert.notNull(sender);
		Assert.notNull(this.actorService.findOne(sender.getId()));
		final Date moment = new Date(System.currentTimeMillis() - 1000);

		res.setSender(sender);
		res.setMoment(moment);

		return res;
	}

	public Message findOne(final int messageId) {
		Assert.isTrue(messageId != 0);
		Message res;

		res = this.messageRepository.findOne(messageId);
		Assert.notNull(res);
		return res;
	}

	public Collection<Message> findAll() {
		Collection<Message> res;

		res = this.messageRepository.findAll();
		Assert.notNull(res);
		return res;
	}

	public Message save(final Message message) {

		Assert.notNull(message);

		final Message sended;
		final Message receivedNotSaved = message;
		final Message received;

		final Actor sender = this.actorService.findByPrincipal();
		Assert.notNull(sender);
		Assert.notNull(this.actorService.findOne(sender.getId()));

		final Actor recipient = message.getRecipient();
		Assert.notNull(recipient);
		Assert.notNull(this.actorService.findOne(recipient.getId()));

		final Folder senderBox = this.folderService.findFolderByNameAndActorId(sender.getId(), "Outbox");
		Assert.notNull(senderBox);

		final Folder recipientBox;

		if (this.isSpam(message)) {
			recipientBox = this.folderService.findFolderByNameAndActorId(recipient.getId(), "Spambox");
			Assert.notNull(recipientBox);
		} else {
			recipientBox = this.folderService.findFolderByNameAndActorId(recipient.getId(), "Inbox");
			Assert.notNull(recipientBox);
		}

		this.actorService.checkSpamWords(message.getBody());
		this.actorService.checkSpamWords(message.getSubject());
		for (final String s : message.getTags())
			this.actorService.checkSpamWords(s);

		sended = this.messageRepository.save(message);
		senderBox.getMessages().add(sended);

		received = this.messageRepository.save(receivedNotSaved);
		recipientBox.getMessages().add(received);

		return sended;
	}

	public void delete(final Message message) {
		Assert.isTrue(message.getId() != 0);
		final Folder trashbox = this.folderService.findFolderByNameAndActorId(this.actorService.findByPrincipal().getId(), "Trashbox");
		if (trashbox.getMessages().contains(message)) {
			trashbox.getMessages().remove(message);
			this.messageRepository.delete(message);
		} else
			this.move(message, trashbox);
	}
	//Other methods--------------------------------------------------
	public Folder findFolderByMessage(final Message message) {
		Assert.notNull(message);
		Folder res;
		res = this.messageRepository.findFolderByMessageId(message.getId());
		Assert.notNull(res);
		return res;
	}

	public void move(final Message message, final Folder newFolder) {
		Assert.notNull(message);
		Assert.notNull(newFolder);

		final Folder oldFolder = this.findFolderByMessage(message);
		Assert.notNull(oldFolder);

		oldFolder.getMessages().remove(message);
		this.folderService.save(oldFolder);

		newFolder.getMessages().add(message);
		this.folderService.save(newFolder);
	}

	public Collection<Message> getMessagesByFolder(final Folder folder) {
		Assert.notNull(folder);
		Assert.isTrue(folder.getId() == this.folderService.findFolderByNameAndActorId(this.actorService.findByPrincipal().getId(), folder.getName()).getId());
		return folder.getMessages();
	}

	public boolean isSpam(final Message message) {
		boolean res = false;
		for (final String spamWord : this.customizationService.getSpamWords()) {
			res = message.getBody().contains(spamWord) || message.getSubject().contains(spamWord);
			if (res)
				break;

		}
		return res;
	}

	public void broadcast(final Message message) {
		Assert.notNull(message);
		final Actor sender = message.getSender();
		Assert.notNull(sender);
		this.actorService.checkAuth(Authority.ADMIN);

		Assert.isTrue(sender.equals(this.actorService.findByPrincipal()));

		final Folder senderFolder = this.folderService.findFolderByNameAndActorId(sender.getId(), "Outbox");
		Assert.notNull(senderFolder);

		final Collection<Actor> actors = this.actorService.findAll();
		actors.remove(sender);

		for (final Actor recipient : actors) {
			final Message m = message;
			m.setRecipient(recipient);
			this.save(m);
		}

	}
}
