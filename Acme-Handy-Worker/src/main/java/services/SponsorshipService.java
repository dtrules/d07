
package services;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.SponsorshipRepository;
import security.Authority;
import domain.Sponsorship;

@Service
@Transactional
public class SponsorshipService {

	//Managed repository ------------------------------------------------

	@Autowired
	private SponsorshipRepository	sponsorshipRepository;

	//@Autowired
	//private SponsorService			sponsorService;

	@Autowired
	private ActorService			actorService;


	// Constructor methods ---------------------------------------------------------
	public SponsorshipService() {
		super();
	}

	//Simple CRUD methods ------------------------

	public Sponsorship create() {

		//this.sponsorService.checkIfSponsor();
		this.actorService.checkAuth(Authority.SPONSOR);

		final Sponsorship s = new Sponsorship();

		return s;
	}

	public Sponsorship save(final Sponsorship s) {
		Assert.notNull(s);
		this.checkSponsorship(s);

		//this.sponsorService.checkIfSponsor();
		this.actorService.checkAuth(Authority.SPONSOR);

		Sponsorship result;
		this.actorService.checkSpamWords(s.getBanner());
		this.actorService.checkSpamWords(s.getTargetPage());
		result = this.sponsorshipRepository.save(s);

		return result;
	}

	public void delete(final Sponsorship s) {

		Assert.notNull(s);
		Assert.isTrue(s.getId() != 0);

		//this.sponsorService.checkIfSponsor();
		this.actorService.checkAuth(Authority.SPONSOR);

		final Sponsorship result = this.sponsorshipRepository.findOne(s.getId());
		Assert.notNull(result);

		this.sponsorshipRepository.delete(result);

	}

	public Sponsorship findOne(final int sponsorshipId) {
		Assert.isTrue(sponsorshipId != 0);
		Sponsorship result;

		result = this.sponsorshipRepository.findOne(sponsorshipId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Sponsorship> findAll() {

		Collection<Sponsorship> result;

		result = this.sponsorshipRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	//Check Sponsorship
	public void checkSponsorship(final Sponsorship sponsorship) {
		Boolean result = true;

		if (sponsorship.getBanner() == null || sponsorship.getTargetPage() == null)
			result = false;

		Assert.isTrue(result);
	}

	public Sponsorship findRandomOneByTutorial(final int tutorialId) {
		final List<Sponsorship> aux = (List<Sponsorship>) this.sponsorshipRepository.findByTutorial(tutorialId);
		Sponsorship res = null;
		final int aleatorio = (int) (Math.random() * aux.size());

		if (aux.size() != 0)
			res = aux.get(aleatorio);
		else
			for (final Sponsorship s : this.sponsorshipRepository.findAll())
				res = s;
		return res;
	}

	public Collection<Sponsorship> findByTutorial(final Integer tutorialId) {
		return this.sponsorshipRepository.findByTutorial(tutorialId);
	}

}
