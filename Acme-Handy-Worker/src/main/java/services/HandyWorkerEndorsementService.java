
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.HandyWorkerEndorsementRepository;
import security.Authority;
import domain.Customization;
import domain.HandyWorkerEndorsement;

@Service
@Transactional
public class HandyWorkerEndorsementService {

	//Managed Repository------------------------------

	@Autowired
	private HandyWorkerEndorsementRepository	handyWorkerEndorsementRepository;

	//Supporting Services-----------------------------
	@Autowired
	private ActorService						actorService;

	@Autowired
	private CustomizationService				customizationService;


	// Constructor methods ---------------------------------------------------------
	public HandyWorkerEndorsementService() {
		super();
	}

	public HandyWorkerEndorsement create() {

		final Collection<String> comments = new ArrayList<String>();
		this.actorService.checkAuth(Authority.HANDYWORKER);

		final HandyWorkerEndorsement res = new HandyWorkerEndorsement();
		res.setComments(comments);

		return res;
	}

	public HandyWorkerEndorsement save(final HandyWorkerEndorsement handyWorkerEndorsement) {
		Assert.notNull(handyWorkerEndorsement);
		this.actorService.checkAuth(Authority.HANDYWORKER);
		final Date moment = new Date(System.currentTimeMillis() - 1000);
		handyWorkerEndorsement.setMoment(moment);
		HandyWorkerEndorsement result = new HandyWorkerEndorsement();
		result = this.handyWorkerEndorsementRepository.save(handyWorkerEndorsement);

		return result;
	}

	public void delete(final HandyWorkerEndorsement handyWorkerEndorsement) {
		Assert.notNull(handyWorkerEndorsement);
		this.actorService.checkAuth(Authority.HANDYWORKER);
		Assert.isTrue(handyWorkerEndorsement.getId() != 0);
		this.handyWorkerEndorsementRepository.delete(handyWorkerEndorsement);

	}

	public HandyWorkerEndorsement findOne(final int handyWorkerEndorsementId) {
		Assert.isTrue(handyWorkerEndorsementId != 0);
		final HandyWorkerEndorsement result = this.handyWorkerEndorsementRepository.findOne(handyWorkerEndorsementId);
		Assert.notNull(result);

		return result;
	}

	public Collection<HandyWorkerEndorsement> findAll() {
		final Collection<HandyWorkerEndorsement> result = this.handyWorkerEndorsementRepository.findAll();

		return result;
	}

	//Score

	public double obtainScore(final HandyWorkerEndorsement handyWorkerEndorsement) {
		double res = 0.0;
		final Customization c = this.customizationService.findCustomization();
		double positiveWords = 0.0;
		double negativeWords = 0.0;
		for (final String s : c.getPositiveWords())
			for (final String s1 : handyWorkerEndorsement.getComments())
				if (s1.toLowerCase().contains(s.toLowerCase()))
					positiveWords++;
		for (final String s : c.getNegativeWords())
			for (final String s1 : handyWorkerEndorsement.getComments())
				if (s1.toLowerCase().contains(s.toLowerCase()))
					negativeWords++;
		res = positiveWords - negativeWords;
		return res;
	}

	//Query
	public Collection<HandyWorkerEndorsement> getEndorsementsFromCustomer(final int customerId) {
		return this.handyWorkerEndorsementRepository.getEndorsementsFromCustomer(customerId);
	}

}
