
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.CreditCardRepository;
import security.Authority;
import domain.CreditCard;
import domain.Customer;

@Service
@Transactional
public class CreditCardService {

	//Managed repository ------------------------------------------------

	@Autowired
	private CreditCardRepository	creditCardRepository;

	//Other repository -----------------------------------------------------

	@Autowired
	private ActorService			actorService;

	@Autowired
	private CustomerService			customerService;


	public CreditCardService() {
		super();
	}

	//Simple CRUD methods ------------------------

	public CreditCard create() {

		this.actorService.checkAuth(Authority.CUSTOMER);
		final CreditCard res = new CreditCard();

		return res;
	}
	public CreditCard save(final CreditCard creditCard) {
		CreditCard result;
		Assert.notNull(creditCard);
		final Customer customer = this.customerService.findByPrincipal();

		Assert.notNull(customer);
		result = this.creditCardRepository.save(creditCard);
		customer.getCreditCards().add(result);

		return result;
	}

	public CreditCard findOne(final int creditCardId) {
		Assert.isTrue(creditCardId != 0);
		CreditCard result;

		result = this.creditCardRepository.findOne(creditCardId);
		Assert.notNull(result);

		return result;
	}

	public Collection<CreditCard> findAll() {

		Collection<CreditCard> result;

		result = this.creditCardRepository.findAll();
		Assert.notNull(result);

		return result;
	}

}
