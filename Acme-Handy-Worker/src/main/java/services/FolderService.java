
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.FolderRepository;
import domain.Actor;
import domain.Folder;
import domain.Message;

@Service
@Transactional
public class FolderService {

	//Managed repositories--------------------------------------
	@Autowired
	private FolderRepository	folderRepository;

	//Supported services----------------------------------------
	@Autowired
	private ActorService		actorService;

	@Autowired
	private MessageService		messageService;


	//Constructor-----------------------------------------------
	public FolderService() {
		super();
	}

	//Simple CRUD methods---------------------------------------
	public Folder create() {
		final Folder res = new Folder();

		final Collection<Message> messages = new ArrayList<Message>();
		res.setMessages(messages);

		return res;
	}

	public Folder findOne(final int folderId) {
		Assert.isTrue(folderId != 0);
		Folder res;
		res = this.folderRepository.findOne(folderId);
		Assert.notNull(res);
		return res;
	}

	public Collection<Folder> findAll() {
		Collection<Folder> res;
		res = this.folderRepository.findAll();
		Assert.notNull(res);
		return res;
	}

	public Folder save(final Folder folder) {
		Assert.notNull(folder);
		Assert.isTrue(this.actorService.findByPrincipal().equals(this.findActorByFolder(folder).getId()));
		final Folder res = this.folderRepository.save(folder);
		return res;
	}

	public Folder saveEdit(final Folder folder) {
		Assert.notNull(folder);
		Assert.isTrue(this.checkNotSystemFolder(folder));
		Assert.isTrue(this.actorService.findByPrincipal().equals(this.findActorByFolder(folder).getId()));
		this.actorService.checkSpamWords(folder.getName());
		final Folder res = this.folderRepository.save(folder);
		return res;
	}

	public Folder saveCreate(final Folder folder) {
		Assert.notNull(folder);
		final Actor actor = this.actorService.findByPrincipal();
		final Collection<Folder> folders = actor.getFolders();
		final Collection<Message> messages = new ArrayList<Message>();
		folder.setMessages(messages);
		for (final Folder f : folders)
			Assert.isTrue(!f.getName().equals(folder.getName()));

		Assert.isTrue(this.checkNotSystemFolder(folder));
		this.actorService.checkSpamWords(folder.getName());

		final Folder res = this.folderRepository.save(folder);
		folders.add(res);
		return res;
	}

	public void delete(final Folder folder) {
		final Folder old = this.findOne(folder.getId());
		Assert.notNull(old);
		Assert.isTrue(this.checkNotSystemFolder(old));
		Assert.isTrue(this.actorService.findByPrincipal().getId() == this.findActorByFolder(folder).getId());
		for (final Message m : old.getMessages())
			this.messageService.delete(m);
		this.folderRepository.delete(folder);
	}

	public void delete(final int folderId) {
		Assert.notNull(folderId);
		final Actor actor = this.actorService.findByPrincipal();
		final Folder folder = this.folderRepository.findOne(folderId);
		Assert.notNull(folder);
		Assert.isTrue(actor.getId() == this.findActorByFolder(folder).getId());
		Assert.isTrue(this.checkNotSystemFolder(folder));
		for (final Message m : folder.getMessages())
			this.messageService.delete(m);
		folder.getMessages().clear();
		actor.getFolders().remove(folder);
		this.actorService.save(actor);
		this.folderRepository.flush();
		this.folderRepository.delete(folder);
	}

	//Other methods---------------------------------------------

	public Collection<Folder> createSystemFolders() {
		final Collection<Folder> res = new ArrayList<Folder>();
		final Collection<Message> messages = new ArrayList<Message>();
		final Folder inbox = new Folder();
		final Folder outbox = new Folder();
		final Folder spambox = new Folder();
		final Folder trashbox = new Folder();

		inbox.setName("Inbox");
		outbox.setName("Outbox");
		spambox.setName("Spambox");
		trashbox.setName("Trashbox");

		inbox.setMessages(messages);
		outbox.setMessages(messages);
		spambox.setMessages(messages);
		trashbox.setMessages(messages);

		this.folderRepository.save(inbox);
		this.folderRepository.save(outbox);

		this.folderRepository.save(spambox);

		this.folderRepository.save(trashbox);

		res.add(inbox);
		res.add(outbox);
		res.add(spambox);
		res.add(trashbox);

		return res;
	}

	public Collection<Folder> findFoldersByActorId(final int actorId) {
		Collection<Folder> res;
		Assert.notNull(actorId);
		Assert.isTrue(actorId > 0);
		res = this.actorService.findOne(actorId).getFolders();
		;
		Assert.notNull(res);
		return res;
	}

	public Folder findFolderByNameAndActorId(final int actorId, final String folderName) {
		Folder res;
		Assert.notNull(actorId);
		Assert.isTrue(actorId != 0);
		Assert.notNull(folderName);
		res = this.folderRepository.findFolderByNameAndActorId(actorId, folderName);
		Assert.notNull(res);
		return res;
	}

	public Actor findActorByFolder(final Folder folder) {
		Actor res;
		Assert.notNull(folder);
		res = this.folderRepository.findActorByFolderId(folder.getId());
		Assert.notNull(res);
		return res;
	}

	public boolean checkIfSystemFolder(final Folder folder) {
		boolean result = false;
		final String name = folder.getName();
		if (name.equals("Inbox") || name.equals("Outbox") || name.equals("Spambox") || name.equals("Trashbox") || name.equals("inbox") || name.equals("outbox") || name.equals("spambox") || name.equals("trashbox"))
			result = true;
		return result;
	}

	public boolean checkNotSystemFolder(final Folder folder) {
		boolean result = true;
		if (this.checkIfSystemFolder(folder))
			result = false;
		return result;
	}

	public Collection<Folder> findSystemFolder(final Actor actor) {
		Assert.isTrue(actor.equals(this.actorService.findByPrincipal()));
		final Collection<Folder> folders = new ArrayList<Folder>();
		for (final Folder f : actor.getFolders())
			if (f.getName().equals("Inbox") || f.getName().equals("Outbox") || f.getName().equals("Spambox") || f.getName().equals("Trashbox"))
				folders.add(f);

		return folders;
	}

	public Folder findFolderByMessageId(final int messageId) {
		return this.folderRepository.findFolderByMessageId(messageId);
	}
}
