
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.CurriculumRepository;
import repositories.HandyWorkerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import security.UserAccountRepository;
import domain.Actor;
import domain.Application;
import domain.Complaint;
import domain.Finder;
import domain.HandyWorker;
import domain.HandyWorkerEndorsement;
import domain.Profile;
import domain.Task;
import domain.Tutorial;

@Service
@Transactional
public class HandyWorkerService {

	//Managed repository --------------------------------

	@Autowired
	private HandyWorkerRepository	handyWorkerRepository;
	@Autowired
	private CurriculumRepository	curriculumRepository;

	//Supported Services ----------------------------------

	@Autowired
	private FolderService			folderService;

	@Autowired
	private ActorService			actorService;

	@Autowired
	private FinderService			finderService;

	@Autowired
	private CurriculumService		curriculumService;

	@Autowired
	private UserAccountRepository	userAccountRepository;


	// Contructor methods
	public HandyWorkerService() {
		super();
	}
	// Simple CRUD methods

	public Collection<HandyWorker> findAll() {
		final Collection<HandyWorker> res = this.handyWorkerRepository.findAll();

		Assert.notNull(res);

		return res;
	}

	public HandyWorker findOne(final int handyWorkerId) {
		Assert.isTrue(handyWorkerId != 0);

		final HandyWorker res = this.handyWorkerRepository.findOne(handyWorkerId);

		Assert.notNull(res);

		return res;
	}

	public HandyWorker create() {

		final HandyWorker res = new HandyWorker();
		final UserAccount userAccount = new UserAccount();

		Collection<Authority> authorities;
		final Authority authority = new Authority();
		authority.setAuthority(Authority.HANDYWORKER);

		authorities = userAccount.getAuthorities();
		authorities.add(authority);
		userAccount.setAuthorities(authorities);
		this.userAccountRepository.save(userAccount);

		res.setUserAccount(userAccount);

		final Collection<HandyWorkerEndorsement> handyWorkerEndorsements = Collections.<HandyWorkerEndorsement> emptySet();
		res.setHandyWorkerEndorsements(handyWorkerEndorsements);

		final Collection<Tutorial> tutorials = Collections.<Tutorial> emptySet();
		res.setTutorials(tutorials);

		final Collection<Application> applications = Collections.<Application> emptySet();
		res.setApplications(applications);

		final Collection<Profile> profiles = new ArrayList<Profile>();
		res.setProfiles(profiles);

		res.setMake("Default");
		res.setIsBanned(false);
		res.setIsSuspicious(false);

		return res;
	}
	public HandyWorker save(final HandyWorker handyWorker) {
		HandyWorker res;

		if (handyWorker.getId() == 0) {
			Assert.notNull(handyWorker);
			final Md5PasswordEncoder encoder = new Md5PasswordEncoder();
			final UserAccount userAccount = handyWorker.getUserAccount();
			final String password = userAccount.getPassword();
			final String hashedPassword = encoder.encodePassword(password, null);
			userAccount.setPassword(hashedPassword);
			final UserAccount ua = this.userAccountRepository.save(userAccount);
			handyWorker.setUserAccount(ua);
			final String make = handyWorker.getName() + " " + handyWorker.getMiddleName() + " " + handyWorker.getSurname();
			handyWorker.setMake(make);
			Finder finder = new Finder();
			finder = this.finderService.save(finder);
			handyWorker.setFinder(finder);
			this.checkHandyWorker(handyWorker);
			handyWorker.setFolders(this.folderService.createSystemFolders());

			res = this.handyWorkerRepository.save(handyWorker);

		} else {
			final String make = handyWorker.getName() + " " + handyWorker.getMiddleName() + " " + handyWorker.getSurname();
			handyWorker.setMake(make);
			this.checkHandyWorker(handyWorker);
			res = this.handyWorkerRepository.save(handyWorker);

		}
		return res;
	}
	public void delete(final HandyWorker handyWorker) {
		Assert.notNull(handyWorker);
		Assert.isTrue(handyWorker.getId() != 0);
		Assert.isTrue(this.handyWorkerRepository.exists(handyWorker.getId()));

		this.actorService.checkAuth(Authority.HANDYWORKER);

		final HandyWorker hw = this.handyWorkerRepository.findOne(handyWorker.getId());

		if (this.curriculumRepository.exists(hw.getCurriculum().getId()))
			this.curriculumService.delete(hw.getCurriculum());

		this.handyWorkerRepository.delete(handyWorker);
	}

	// Finds the actual handyWorker
	public HandyWorker findByPrincipal() {
		HandyWorker result;

		final Actor principal = this.actorService.findByPrincipal();

		result = this.handyWorkerRepository.findOne(principal.getId());
		//Assert.notNull(result);

		return result;
	}

	// Find a handyWorker by his or her username
	public HandyWorker findHandyWorkerByUsername(final String username) {
		return this.handyWorkerRepository.findHandyWorkerByUsername(username);
	}

	// Find handyWorker by useraccount
	public HandyWorker findCustomerByUserAccount(final UserAccount userAccount) {
		return this.handyWorkerRepository.findByUserAccount(userAccount);
	}

	public HandyWorker findHandyWorkerByApplicationId(final int applicationId) {
		return this.handyWorkerRepository.findHandyWorkerByApplicationId(applicationId);
	}

	//Check handyWorker
	public void checkHandyWorker(final HandyWorker handyWorker) {
		Boolean result = true;

		if (handyWorker.getApplications() == null || handyWorker.getEmail() == null || handyWorker.getFolders() == null || handyWorker.getName() == null || handyWorker.getSurname() == null || handyWorker.getFinder() == null
			|| handyWorker.getHandyWorkerEndorsements() == null)
			result = false;

		Assert.isTrue(result);
	}

	public void checkIfHandyWorker() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.HANDYWORKER))
				res = true;
		Assert.isTrue(res);
	}

	public Collection<HandyWorker> findHandyWorkerByAcceptedApplicationsOrderedByComplaint() {
		Collection<HandyWorker> res;
		res = this.handyWorkerRepository.findHandyWorkerByAcceptedApplicationsOrderedByComplaint();
		return res;
	}

	public Collection<HandyWorker> top3HandyWorkerTermsOfComplaints() {
		Collection<HandyWorker> res;
		res = this.handyWorkerRepository.top3HandyWorkerTermsOfComplaints();
		return res;
	}

	public Collection<Complaint> findComplaintsByHandyWorker(final HandyWorker handyWorker) {
		final Collection<Complaint> complaints = new ArrayList<Complaint>();
		final Collection<Task> tasks = new ArrayList<Task>();
		for (final Application a : handyWorker.getApplications())
			tasks.add(a.getTask());
		for (final Task t : tasks)
			complaints.addAll(t.getComplaints());

		return complaints;
	}
}
