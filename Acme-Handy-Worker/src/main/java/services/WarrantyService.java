
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.WarrantyRepository;
import security.Authority;
import domain.Warranty;

@Service
@Transactional
public class WarrantyService {

	//Managed Repository------------------------------

	@Autowired
	private WarrantyRepository	warrantyRepository;

	//Supporting Services-----------------------------

	@Autowired
	private ActorService		actorService;


	// Constructor methods ---------------------------------------------------------
	public WarrantyService() {
		super();
	}

	//Simple CRUD methods-----------------------------

	/*
	 * An actor who is authenticated as an administrator must be able to:
	 * Manage the catalogue of warranties, which includes listing, showing, creating, up-dating, and deleting them.
	 * A warranty can be updated or deleted as long as it is saved in draft mode. Once its saved in final mode,
	 * it cannot be edited or deleted. Only warranties that are saved in final mode can be referenced by fix-up tasks.
	 */

	//lo crea el administrator
	public Warranty create() {
		this.actorService.checkAuth(Authority.ADMIN);

		final Warranty res = new Warranty();
		res.setDraftMode(true);

		return res;
	}

	public Warranty save(final Warranty warranty) {
		Assert.notNull(warranty);
		this.checkWarranty(warranty);
		Warranty result = new Warranty();

		this.actorService.checkAuth(Authority.ADMIN);
		this.actorService.checkSpamWords(warranty.getLaws());
		this.actorService.checkSpamWords(warranty.getTerms());
		this.actorService.checkSpamWords(warranty.getTitle());
		result = this.warrantyRepository.save(warranty);

		return result;
	}

	//Se puede borrar si esta modo borrador
	public void delete(final Warranty warranty) {
		this.actorService.checkAuth(Authority.ADMIN);
		Assert.notNull(warranty);
		final Warranty w = this.findOne(warranty.getId());

		this.actorService.checkAuth(Authority.ADMIN);
		Assert.isTrue(w.getDraftMode());
		this.warrantyRepository.delete(w.getId());
	}

	public Warranty findOne(final int warrantyId) {
		Assert.isTrue(warrantyId != 0);
		final Warranty result = this.warrantyRepository.findOne(warrantyId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Warranty> findAll() {
		final Collection<Warranty> result = this.warrantyRepository.findAll();

		return result;
	}

	public boolean exists(final int warrantyId) {
		return this.warrantyRepository.exists(warrantyId);
	}

	public Collection<Warranty> findWarrantiesNoDraft() {
		final Collection<Warranty> result = this.warrantyRepository.findWarrantiesNoDraft();

		return result;
	}

	//Checkers------------------------------

	//Check Warranty
	public void checkWarranty(final Warranty warranty) {
		Boolean result = true;

		if (warranty.getTitle() == null || warranty.getLaws() == null || warranty.getTerms() == null)
			result = false;

		Assert.isTrue(result);
	}

}
