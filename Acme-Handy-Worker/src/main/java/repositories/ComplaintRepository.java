
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Complaint;

@Repository
public interface ComplaintRepository extends JpaRepository<Complaint, Integer> {

	@Query("select co from Customer c join c.tasks t join t.complaints co where c.id=?1")
	Collection<Complaint> findComplaintByCustomer(int customerId);
}
