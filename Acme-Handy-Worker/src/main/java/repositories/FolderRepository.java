
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Actor;
import domain.Folder;

@Repository
public interface FolderRepository extends JpaRepository<Folder, Integer> {

	@Query("select f from Actor a join a.folders f where f.name=?2 and a.id=?1")
	Folder findFolderByNameAndActorId(int actorId, String folderName);

	@Query("select a from Actor a join a.folders f where f.id = ?1")
	Actor findActorByFolderId(int folderId);

	@Query("select f from Folder f join f.messages m where m.id = ?1")
	Folder findFolderByMessageId(int messageId);

}
